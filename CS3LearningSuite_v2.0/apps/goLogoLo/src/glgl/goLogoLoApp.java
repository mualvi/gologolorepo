/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl;

import djf.AppTemplate;
import djf.components.AppClipboardComponent;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import djf.components.AppWorkspaceComponent;
import java.util.Locale;
import glgl.data.goLoData;
import glgl.files.goLoFiles;
import glgl.clipboard.goLoClipboard;
import glgl.workspace.goLoWorkspace;
import static javafx.application.Application.launch;

/**
 *
 * @author musta
 */
public class goLogoLoApp extends AppTemplate {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }

    @Override
    public AppClipboardComponent buildClipboardComponent(AppTemplate app) {
        return new goLoClipboard(this);
    }

    @Override
    public AppDataComponent buildDataComponent(AppTemplate app) {
        return new goLoData(this);
    }

    @Override
    public AppFileComponent buildFileComponent() {
        return new goLoFiles();
    }

    @Override
    public AppWorkspaceComponent buildWorkspaceComponent(AppTemplate app) {
        return new goLoWorkspace(this);
    }

}
