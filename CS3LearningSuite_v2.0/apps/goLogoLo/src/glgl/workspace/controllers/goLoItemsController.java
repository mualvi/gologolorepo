/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.workspace.controllers;

import glgl.data.components.goLoCircleItem;
import glgl.data.components.goLoImageItem;
import glgl.data.components.goLoRectangleItem;
import glgl.data.components.goLoTextItem;
import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import glgl.transactions.AddItem_Transaction;
import glgl.transactions.Boolean_Transaction;
import glgl.transactions.Choice_Transaction;
import glgl.transactions.DecreaseFontSize_Transaction;
import glgl.transactions.EditName_Transaction;
import glgl.transactions.EditTextItem_Transaction;
import glgl.transactions.IncreaseFontSize_Transaction;
import glgl.transactions.MoveDownItem_Transaction;
import glgl.transactions.MoveUpItem_Transaction;
import glgl.transactions.RemoveItem_Transaction;
import glgl.transactions.ResizePane_Transaction;
import glgl.transactions.Slider_Transaction;
import glgl.workspace.dialogs.goLoItemDialog;
import glgl.workspace.goLoWorkspace;
import java.awt.image.BufferedImage;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;

/**
 *
 * @author musta
 */
public class goLoItemsController {

    goLogoLoApp app;
    goLoItemDialog itemDialog;

    public goLoItemsController(goLogoLoApp initApp) {
        app = initApp;
        itemDialog = new goLoItemDialog(app);
    }

    public void processEditName() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();
            itemDialog.showEditNameDialog(item);

            if (itemDialog.getNewName() != null) {
                String newName = itemDialog.getNewName();

                EditName_Transaction transaction = new EditName_Transaction(app, item, newName);
                app.processTransaction(transaction);
            }

        }
    }

    public void processMoveUpComponent() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();
            MoveUpItem_Transaction transaction = new MoveUpItem_Transaction(app, item);
            app.processTransaction(transaction);
        }

    }

    public void processMoveDownComponent() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();
            MoveDownItem_Transaction transaction = new MoveDownItem_Transaction(app, item);
            app.processTransaction(transaction);
        }

    }

    public void processAddTextComponent() {

        itemDialog.showAddTextDialog();
        goLoItem newTextItem = itemDialog.getNewItem();

        if (newTextItem != null) {
            goLoData data = (goLoData) app.getDataComponent();
            AddItem_Transaction transaction = new AddItem_Transaction(data, newTextItem);
            app.processTransaction(transaction);

        }
    }

    public void processEditTextItem() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isTextItemSelected()) {

            goLoTextItem item = (goLoTextItem)data.getSelectedItem();
            String OGtext = item.getComponent().getText(); 
            itemDialog.showEditTextDialog(data.getSelectedItem());

            if (itemDialog.getEditTextItemString() != null) {
                String newStringForTextItem =  itemDialog.getEditTextItemString();

                EditTextItem_Transaction transaction = new EditTextItem_Transaction(data, item, newStringForTextItem, OGtext);
                app.processTransaction(transaction);
            }
        }
    }

    public void processAddImageComponent() {
        goLoData data = (goLoData) app.getDataComponent();
        itemDialog.showAddImageDialog();

        if (itemDialog.getImage() != null) {
            BufferedImage buff = itemDialog.getImage();
            goLoItem imageItem = new goLoImageItem(buff); 

            AddItem_Transaction transaction = new AddItem_Transaction(data, imageItem);
            app.processTransaction(transaction);
        }
    }

    public void processAddRectangleComponent() {
        //will be applicable to circle and triangle
        goLoRectangleItem addRect = new goLoRectangleItem();

        goLoData data = (goLoData) app.getDataComponent();
        AddItem_Transaction transaction = new AddItem_Transaction(data, addRect);
        app.processTransaction(transaction);
    }

    public void processAddCircleComponent() {
        goLoCircleItem addCirc = new goLoCircleItem();

        goLoData data = (goLoData) app.getDataComponent();
        AddItem_Transaction transaction = new AddItem_Transaction(data, addCirc);
        app.processTransaction(transaction);
    }

    public void processRemoveComponent() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem itemToRemove = data.getSelectedItem();
            RemoveItem_Transaction transaction = new RemoveItem_Transaction(app, itemToRemove);
            app.processTransaction(transaction);
        }
    }

    public void processSliderEvent(Slider slider, int value, String control) {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();
            data.editSliderValue(item, control, value, slider);

        }
    }

    public void processSliderEventTransaction(Slider slider, double value, String control) {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();

            Slider_Transaction transaction = new Slider_Transaction(app, item, control, value, slider);
            app.processTransaction(transaction);

        }
    }

    public void processChoiceEvent(ComboBoxBase comboBox, String value, String controlName) {
        goLoData data = (goLoData) app.getDataComponent();

        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();

            Choice_Transaction transaction = new Choice_Transaction(app, item, controlName, value, comboBox);
            app.processTransaction(transaction);

        }
    }

    public void processBooleanTransaction(String controlSource) {
        goLoData data = (goLoData) app.getDataComponent();

        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();

            Boolean_Transaction transaction = new Boolean_Transaction(app, item, controlSource);
            app.processTransaction(transaction);

        }

    }

    public void processResizeImage() {
        Pane center = ((goLoWorkspace) (app.getWorkspaceComponent())).getCenter().getCenter();
        goLoData data = (goLoData) app.getDataComponent();
        itemDialog.showResizeDialog(center);

        double scaleY = itemDialog.getPaneHeight();
        double scaleX = itemDialog.getPaneWidth();

        ResizePane_Transaction transaction = new ResizePane_Transaction(data, center, scaleX, scaleY);
        app.processTransaction(transaction);

    }

    public void processIncreaseFontSizeTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isTextItemSelected()) {
            IncreaseFontSize_Transaction transaction = new IncreaseFontSize_Transaction(data, data.getSelectedItem());
            app.processTransaction(transaction);
        }

    }

    public void processDecreaseFontSizeTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isTextItemSelected()) {
            DecreaseFontSize_Transaction transaction = new DecreaseFontSize_Transaction(data, data.getSelectedItem());
            app.processTransaction(transaction);
        }
    }

}
