/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.workspace.controllers;

import djf.AppTemplate;
import djf.modules.AppGUIModule;
import glgl.data.goLoItem;
import static glgl.goLoPropertyType.GLGL_ITEMS_TABLE_VIEW;
import glgl.goLogoLoApp;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * -This class needs to keep track of names, orders, and types -Names need to be
 * editable via layerToolbar -Default name can be name of var OR empty...u
 * decide
 *
 * -Should have instance of TableView<ObservableList>
 * -should also have a SelectionModel to identify selected componentItems in the
 * list -SingleSelectionModel ONLY -Selecting an item will enable the 3 buttons
 * on the layers toolbar...but that can be defined in the foolproof class
 *
 * -method to reorder table if user chooses to move component up or down -method
 * to rename the component...which should show dialog
 *
 * -
 *
 * @author musta
 */
public class goLoTableController {

    goLogoLoApp app;

    public goLoTableController(AppTemplate initApp) {
        app = (goLogoLoApp) initApp;
    }

    public void processChangeTableSize() {
        AppGUIModule gui = app.getGUIModule();
        TableView<goLoItem> itemsTable = (TableView) gui.getGUINode(GLGL_ITEMS_TABLE_VIEW);
        ObservableList columns = itemsTable.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = (TableColumn) columns.get(i);
            column.setMinWidth(itemsTable.widthProperty().getValue() / columns.size());
            column.setMaxWidth(itemsTable.widthProperty().getValue() / columns.size());
        }
    }

}
