/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * This class lists all CSS style types for this application. These
 * are used by JavaFX to apply style properties to controls like
 * buttons, labels, and panes.
 *
 * @author mustafa
 * @author ?
 * @version 1.0
 *
 *
 */
package glgl.workspace.style;

/**
 *
 * @author musta
 */
public class GLStyle {

    public static final String EMPTY_TEXT = "";
    public static final int BUTTON_TAG_WIDTH = 75;

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS M3Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    // NOTE THAT FOUR CLASS STYLES ALREADY EXIST:
    // top_toolbar, toolbar, toolbar_text_button, toolbar_icon_button
    public static final String CLASS_GLGL_PANE = "glgl_pane";
    public static final String CLASS_GLGL_BOX = "glgl_box";
    public static final String CLASS_GLGL_BIG_HEADER = "glgl_big_header";
    public static final String CLASS_GLGL_SMALL_HEADER = "glgl_small_header";
    public static final String CLASS_GLGL_PROMPT = "glgl_prompt";
    public static final String CLASS_GLGL_TEXT_FIELD = "glgl_text_field";
    public static final String CLASS_GLGL_ICON_BUTTON = "glgl_icon_button";
    public static final String CLASS_GLGL_TABLE = "glgl_table";
    public static final String CLASS_GLGL_COLUMN = "glgl_column";
    public static final String CLASS_GLGL_SLIDER = "glgl_slider";
    public static final String CLASS_GLGL_CHECKBOX = "glgl_checkbox";
    public static final String CLASS_GLGL_LABEL = "glgl_label";
    public static final String CLASS_GLGL_COMBOBOX = "glgl_combo_box";
    public static final String CLASS_GLGL_COLOR_PICKER = "glgl_color_picker";

    public static final String CLASS_GLGL_TOOLBAR = "glgl_toolbar";
    public static final String CLASS_GLGL_TOOLBAR_COMPONENT = "glgl_component_toolbar";
    public static final String CLASS_GLGL_TOOLBAR_FONT = "glgl_font_toolbar";
    public static final String CLASS_GLGL_TOOLBAR_BORDER = "glgl_border_toolbar";
    public static final String CLASS_GLGL_TOOLBAR_GRADIENT = "glgl_gradient_toolbar";

    // STYLE CLASSES FOR THE ADD/EDIT ITEM DIALOG
    public static final String CLASS_GLGL_DIALOG_GRID = "glgl_dialog_grid";
    public static final String CLASS_GLGL_DIALOG_HEADER = "glgl_dialog_header";
    public static final String CLASS_GLGL_DIALOG_PROMPT = "glgl_dialog_prompt";
    public static final String CLASS_GLGL_DIALOG_TEXT_FIELD = "glgl_dialog_text_field";
    public static final String CLASS_GLGL_DIALOG_CHECK_BOX = "glgl_dialog_check_box";
    public static final String CLASS_GLGL_DIALOG_BUTTON = "glgl_dialog_button";
    public static final String CLASS_GLGL_DIALOG_PANE = "glgl_dialog_pane";

}
