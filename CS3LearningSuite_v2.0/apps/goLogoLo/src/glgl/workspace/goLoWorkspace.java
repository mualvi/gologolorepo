package glgl.workspace;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.DISABLED;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import static djf.ui.style.DJFStyle.CLASS_DJF_ICON_BUTTON;
import static djf.ui.style.DJFStyle.CLASS_DJF_TOOLBAR_PANE;
import static djf.ui.style.DJFStyle.CLASS_DJF_TOP_TOOLBAR;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import glgl.goLogoLoApp;
import static glgl.goLoPropertyType.GLGL_FOOLPROOF_SETTINGS;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_BIG_HEADER;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_ICON_BUTTON;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_COLUMN;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_TABLE;
import static glgl.goLoPropertyType.GLGL_LAYER_BUTTONS_PANE;
import static glgl.goLoPropertyType.GLGL_EDIT_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_RESET_VIEWPORT_BUTTON;
import static glgl.goLoPropertyType.GLGL_ZOOM_IN_BUTTON;
import static glgl.goLoPropertyType.GLGL_ZOOM_OUT_BUTTON;
import static glgl.goLoPropertyType.GLGL_RESIZE_LOGO_BUTTON;
import static glgl.goLoPropertyType.GLGL_REMOVE_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_ITEMS_TABLE_VIEW;
import static glgl.goLoPropertyType.GLGL_MOVE_DOWN_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_MOVE_UP_ITEM_BUTTON;
import glgl.workspace.controllers.goLoTableController;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_BOX;
import glgl.data.goLoData;
import glgl.data.goLoItem;
import static glgl.goLoPropertyType.CYCLE_OPTIONS;
import static glgl.goLoPropertyType.DEFAULT_CYCLE;
import static glgl.goLoPropertyType.DEFAULT_FONT_SIZE;
import static glgl.goLoPropertyType.DEFAULT_FONT_STYLE;
import static glgl.goLoPropertyType.FONT_SIZE_OPTIONS;
import static glgl.goLoPropertyType.FONT_STYLE_OPTIONS;
import static glgl.goLoPropertyType.GLGL_BOLD_BUTTON;
import static glgl.goLoPropertyType.GLGL_BORDER_COLOR_PICKER;
import static glgl.goLoPropertyType.GLGL_BORDER_COLOR_LABEL;
import static glgl.goLoPropertyType.GLGL_BORDER_RADIUS_LABEL;
import static glgl.goLoPropertyType.GLGL_BORDER_STROKE_PANE;
import static glgl.goLoPropertyType.GLGL_BORDER_THICKNESS_LABEL;
import static glgl.goLoPropertyType.GLGL_BORDER_THICKNESS_SLIDER;
import static glgl.goLoPropertyType.GLGL_CENTER_X_LABEL;
import static glgl.goLoPropertyType.GLGL_CENTER_X_SLIDER;
import static glgl.goLoPropertyType.GLGL_CENTER_Y_LABEL;
import static glgl.goLoPropertyType.GLGL_CENTER_Y_SLIDER;
import static glgl.goLoPropertyType.GLGL_CIRCLE_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_COLOR_GRADIENT_LABEL;
import static glgl.goLoPropertyType.GLGL_COLOR_GRADIENT_PANE;
import static glgl.goLoPropertyType.GLGL_COMPONENTS_PANE;
import static glgl.goLoPropertyType.GLGL_CYCLE_METHOD_COMBO_BOX;
import static glgl.goLoPropertyType.GLGL_CYCLE_METHOD_LABEL;
import static glgl.goLoPropertyType.GLGL_DECREASE_FONT_BUTTON;
import static glgl.goLoPropertyType.GLGL_FOCUS_ANGLE_LABEL;
import static glgl.goLoPropertyType.GLGL_FOCUS_ANGLE_SLIDER;
import static glgl.goLoPropertyType.GLGL_FOCUS_DISTANCE_LABEL;
import static glgl.goLoPropertyType.GLGL_FOCUS_DISTANCE_SLIDER;
import static glgl.goLoPropertyType.GLGL_FONT_PANE;
import static glgl.goLoPropertyType.GLGL_FONT_SIZE_COMBO_BOX;
import static glgl.goLoPropertyType.GLGL_FONT_STYLE_COMBO_BOX;
import static glgl.goLoPropertyType.GLGL_IMAGE_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_INCREASE_FONT_BUTTON;
import static glgl.goLoPropertyType.GLGL_ITALICS_BUTTON;
import static glgl.goLoPropertyType.GLGL_LEFT_BORDER_PANE;
import static glgl.goLoPropertyType.GLGL_NAME_COLUMN;
import static glgl.goLoPropertyType.GLGL_ORDER_COLUMN;
import static glgl.goLoPropertyType.GLGL_RADIUS_LABEL;
import static glgl.goLoPropertyType.GLGL_RADIUS_SLIDER;
import static glgl.goLoPropertyType.GLGL_RECTANGLE_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_RIGHT_BORDER_PANE;
import static glgl.goLoPropertyType.GLGL_STOP_0_COLOR_PICKER;
import static glgl.goLoPropertyType.GLGL_STOP_0_COLOR_LABEL;
import static glgl.goLoPropertyType.GLGL_STOP_1_COLOR_PICKER;
import static glgl.goLoPropertyType.GLGL_STOP_1_COLOR_LABEL;
import static glgl.goLoPropertyType.GLGL_TEXT_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_TOGGLE_SNAP_CHECKBOX;
import static glgl.goLoPropertyType.GLGL_TOGGLE_SNAP_LABEL;
import static glgl.goLoPropertyType.GLGL_TYPE_COLUMN;
import static glgl.goLoPropertyType.GLGL_UNDERLINE_BUTTON;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_CHECKBOX;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_COMBOBOX;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_SLIDER;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_LABEL;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_TOOLBAR;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import glgl.workspace.controllers.goLoItemsController;
import glgl.workspace.foolproof.goLoFoolproof;
import glgl.workspace.goLoCenterPane;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_COLOR_PICKER;
import java.util.HashMap;
import javafx.scene.control.ColorPicker;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author musta
 */
public class goLoWorkspace extends AppWorkspaceComponent {

    AppNodesBuilder glglBuilder = app.getGUIModule().getNodesBuilder();
    goLoItemsController itemsController = new goLoItemsController((goLogoLoApp) app);
    goLoCenterPane GLC;
    double OGvalue;
    HashMap<String, Node> asStringToNode;

    public goLoWorkspace(goLogoLoApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        initFoolproofDesign();
        
    }

    public goLoCenterPane getCenter() {
        return GLC;
    }

    public void hashStringToNode(Pane p) {
        p.getChildren().forEach(e -> {
            asStringToNode.put(e.toString(), e);
        });
    }

    public Node getNode(String asString) {
        return asStringToNode.get(asString);
    }

    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder glglBuilder = app.getGUIModule().getNodesBuilder();

        // TOP BORDER (FROM GUI MODULE SO WE USE DJF STYLINGS)
        initTopBorder();

        // AND PUT APPLICATION UI OBJECTS IN THE WORKSPACE
        workspace = new BorderPane();
        ((BorderPane) workspace).setLeft(initLeftBorder());
        ((BorderPane) workspace).setRight(initRightBorder());

        //MAKE CENTER
        GLC = new goLoCenterPane((goLogoLoApp) app);
        ((BorderPane) workspace).setCenter(GLC.getCenter());
        
        //To listen for any new transactions, and enable save button
        app.getTPS().getListener().addListener(e -> {
            app.getFileModule().markAsEdited(true);
        });
                
    }

    void initTopBorder() {
        FlowPane topToolbarPane = app.getGUIModule().getTopToolbarPane();
        topToolbarPane.toFront();
        ToolBar navigationToolbar = new ToolBar();
        topToolbarPane.getChildren().add(3, navigationToolbar);

        Button resetViewportButton = glglBuilder.buildIconButton(GLGL_RESET_VIEWPORT_BUTTON, null, navigationToolbar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button zoomInButton = glglBuilder.buildIconButton(GLGL_ZOOM_IN_BUTTON, null, navigationToolbar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button zoomOutButton = glglBuilder.buildIconButton(GLGL_ZOOM_OUT_BUTTON, null, navigationToolbar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button resizeLogoButton = glglBuilder.buildIconButton(GLGL_RESIZE_LOGO_BUTTON, null, navigationToolbar, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        CheckBox snapCheckBox = glglBuilder.buildCheckBox(GLGL_TOGGLE_SNAP_CHECKBOX, null, navigationToolbar, CLASS_GLGL_CHECKBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label snapCheckBoxLabel = glglBuilder.buildLabel(GLGL_TOGGLE_SNAP_LABEL, null, navigationToolbar, CLASS_GLGL_CHECKBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        topToolbarPane.getStyleClass().add(CLASS_DJF_TOP_TOOLBAR);

        resetViewportButton.setOnAction(e -> {
            GLC.center.setScaleX(1.0);
            GLC.center.setScaleY(1.0);
        });

        resizeLogoButton.setOnAction(e -> {
            itemsController.processResizeImage();
        });

        zoomInButton.setOnAction(e -> {
            GLC.center.setScaleX(1.0 * 1.2);
            GLC.center.setScaleY(1.0 * 1.2);
        });

        zoomOutButton.setOnAction(e -> {
            GLC.center.setScaleX(1.0 * .8);
            GLC.center.setScaleY(1.0 * .8);
        });

        for (Node toolbar : topToolbarPane.getChildren()) {
            ObservableList<String> styleClasses = toolbar.getStyleClass();
            styleClasses.add(CLASS_DJF_TOOLBAR_PANE);
        }
    }

    Pane initLeftBorder() {
        VBox leftBorderPane = glglBuilder.buildVBox(GLGL_LEFT_BORDER_PANE, null, null, CLASS_GLGL_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        initTable(leftBorderPane);
        initLayerToolbar(leftBorderPane);
        return leftBorderPane;
    }

    Pane initRightBorder() {
        VBox rightBorderPane = glglBuilder.buildVBox(GLGL_RIGHT_BORDER_PANE, null, null, CLASS_GLGL_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        initComponentToolbar(rightBorderPane);
        initFontToolbar(rightBorderPane);
        initBorderToolbar(rightBorderPane);
        initColorGradientToolbar(rightBorderPane);
        return rightBorderPane;
    }

    void initTable(Pane parent) {
        TableView<goLoItem> itemsTable = glglBuilder.buildTableView(GLGL_ITEMS_TABLE_VIEW, parent, null, CLASS_GLGL_TABLE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, true);
        TableColumn orderColumn = glglBuilder.buildTableColumn(GLGL_ORDER_COLUMN, itemsTable, CLASS_GLGL_COLUMN);
        TableColumn nameColumn = glglBuilder.buildTableColumn(GLGL_NAME_COLUMN, itemsTable, CLASS_GLGL_COLUMN);
        TableColumn typeColumn = glglBuilder.buildTableColumn(GLGL_TYPE_COLUMN, itemsTable, CLASS_GLGL_COLUMN);
        // SPECIFY THE TYPES FOR THE COLUMNS
        orderColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("order"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));

        goLoTableController iTC = new goLoTableController(app);

        itemsTable.setOnMouseClicked(e -> {
            goLoData data = (goLoData) app.getDataComponent();
            getCenter().processHighlight();

            if ((e.getClickCount() == 2)
                    && data.isItemSelected()
                    && (data.getSelectedItem().getType().equals("goLoTextItem"))) {
                itemsController.processEditTextItem();
            }
            app.getFoolproofModule().updateAll();
        });

        itemsTable.widthProperty().addListener(e -> {
            iTC.processChangeTableSize();
        });

    }

    void initLayerToolbar(Pane parent) {
        HBox layerButtonsPane = glglBuilder.buildHBox(GLGL_LAYER_BUTTONS_PANE, parent, null, CLASS_GLGL_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button editItemButton = glglBuilder.buildIconButton(GLGL_EDIT_ITEM_BUTTON, layerButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);
        Button moveUpItemButton = glglBuilder.buildIconButton(GLGL_MOVE_UP_ITEM_BUTTON, layerButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);
        Button moveDownItemButton = glglBuilder.buildIconButton(GLGL_MOVE_DOWN_ITEM_BUTTON, layerButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);

        editItemButton.setOnAction(e -> {
            itemsController.processEditName();
        });
        moveUpItemButton.setOnAction(e -> {
            itemsController.processMoveUpComponent();
        });
        moveDownItemButton.setOnAction(e -> {
            itemsController.processMoveDownComponent();
        });
    }

    void initComponentToolbar(Pane parent) {
        //R-Components
        HBox componentButtonsPane = glglBuilder.buildHBox(GLGL_COMPONENTS_PANE, parent, null, CLASS_GLGL_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button textItemButton = glglBuilder.buildIconButton(GLGL_TEXT_ITEM_BUTTON, componentButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button imageItemButton = glglBuilder.buildIconButton(GLGL_IMAGE_ITEM_BUTTON, componentButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button rectangleItemButton = glglBuilder.buildIconButton(GLGL_RECTANGLE_ITEM_BUTTON, componentButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button circleItemButton = glglBuilder.buildIconButton(GLGL_CIRCLE_ITEM_BUTTON, componentButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        //       Button triangleItemButton = glglBuilder.buildIconButton(GLGL_TRIANGLE_ITEM_BUTTON, componentButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);
        Button removeItemButton = glglBuilder.buildIconButton(GLGL_REMOVE_ITEM_BUTTON, componentButtonsPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);

        textItemButton.setOnAction(e -> {
            itemsController.processAddTextComponent();
        });

        rectangleItemButton.setOnAction(e -> {
            itemsController.processAddRectangleComponent();
        });

        circleItemButton.setOnAction(e -> {
            itemsController.processAddCircleComponent();
        });

        imageItemButton.setOnAction(e -> {
            itemsController.processAddImageComponent();
        });

        removeItemButton.setOnAction(e -> {
            itemsController.processRemoveComponent();
        });

    }

    void initFontToolbar(Pane parent) {
        FlowPane fontPane = glglBuilder.buildFlowPane(GLGL_FONT_PANE, parent, null, CLASS_GLGL_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);
        ComboBox fontStyleBox = glglBuilder.buildComboBox(GLGL_FONT_STYLE_COMBO_BOX, FONT_STYLE_OPTIONS, DEFAULT_FONT_STYLE, fontPane, null, CLASS_GLGL_COMBOBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ComboBox fontSizeBox = glglBuilder.buildComboBox(GLGL_FONT_SIZE_COMBO_BOX, FONT_SIZE_OPTIONS, DEFAULT_FONT_SIZE, fontPane, null, CLASS_GLGL_COMBOBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button boldButton = glglBuilder.buildIconButton(GLGL_BOLD_BUTTON, fontPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button italicsButton = glglBuilder.buildIconButton(GLGL_ITALICS_BUTTON, fontPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button incFontButton = glglBuilder.buildIconButton(GLGL_INCREASE_FONT_BUTTON, fontPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button decFontButton = glglBuilder.buildIconButton(GLGL_DECREASE_FONT_BUTTON, fontPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button underlineButton = glglBuilder.buildIconButton(GLGL_UNDERLINE_BUTTON, fontPane, null, CLASS_GLGL_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

        boldButton.setOnAction(e -> {
            itemsController.processBooleanTransaction("bold");
        });
        italicsButton.setOnAction(e -> {
            itemsController.processBooleanTransaction("italic");
        });
        incFontButton.setOnAction(e -> {
            itemsController.processIncreaseFontSizeTransaction();
        });
        decFontButton.setOnAction(e -> {
            itemsController.processDecreaseFontSizeTransaction();

        });
        underlineButton.setOnAction(e -> {
            itemsController.processBooleanTransaction("underline");
        });

        registerChoiceHandler(fontStyleBox, "fontStyle");
        registerChoiceHandler(fontSizeBox, "fontSize");

        // hashStringToNode(fontPane); 
    }

    void initBorderToolbar(Pane parent) {
        VBox borderStylePane = glglBuilder.buildVBox(GLGL_BORDER_STROKE_PANE, parent, null, CLASS_GLGL_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label borderThicknessLabel = glglBuilder.buildLabel(GLGL_BORDER_THICKNESS_LABEL, borderStylePane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider borderThicknessSlider = glglBuilder.buildSlider(GLGL_BORDER_THICKNESS_SLIDER, borderStylePane, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label borderColorLabel = glglBuilder.buildLabel(GLGL_BORDER_COLOR_LABEL, borderStylePane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker borderColorPicker = glglBuilder.buildColorPicker(GLGL_BORDER_COLOR_PICKER, borderStylePane, null, CLASS_GLGL_COLOR_PICKER, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label borderRadiusLabel = glglBuilder.buildLabel(GLGL_BORDER_RADIUS_LABEL, borderStylePane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider borderRadiusSlider = glglBuilder.buildSlider(GLGL_BORDER_THICKNESS_SLIDER, borderStylePane, null, CLASS_GLGL_SLIDER, 0, 10, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

        registerSliderHandler(borderThicknessSlider, "borderThickness");
        registerSliderHandler(borderRadiusSlider, "borderRadius");
        registerColorPickerHandler(borderColorPicker, "borderColor");
      // hashStringToNode(borderStylePane); 
    }

    void initColorGradientToolbar(Pane parent) {
        VBox colorGradientPane = glglBuilder.buildVBox(GLGL_COLOR_GRADIENT_PANE, parent, null, CLASS_GLGL_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label colorGradientLabel = glglBuilder.buildLabel(GLGL_COLOR_GRADIENT_LABEL, colorGradientPane, null, CLASS_GLGL_BIG_HEADER, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label focusAngleLabel = glglBuilder.buildLabel(GLGL_FOCUS_ANGLE_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider focusAngleSlider = glglBuilder.buildSlider(GLGL_FOCUS_ANGLE_SLIDER, colorGradientPane, null, CLASS_GLGL_SLIDER, 0, 100, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label focusDistanceLabel = glglBuilder.buildLabel(GLGL_FOCUS_DISTANCE_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider focusDistanceSlider = glglBuilder.buildSlider(GLGL_FOCUS_DISTANCE_SLIDER, colorGradientPane, null, CLASS_GLGL_SLIDER, 0, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        restructSlider(focusDistanceSlider);
        Label centerXLabel = glglBuilder.buildLabel(GLGL_CENTER_X_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider centerXSlider = glglBuilder.buildSlider(GLGL_CENTER_X_SLIDER, colorGradientPane, null, CLASS_GLGL_SLIDER, 0, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        restructSlider(centerXSlider);
        Label centerYLabel = glglBuilder.buildLabel(GLGL_CENTER_Y_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider centerYSlider = glglBuilder.buildSlider(GLGL_CENTER_Y_SLIDER, colorGradientPane, null, CLASS_GLGL_SLIDER, 0, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        restructSlider(centerYSlider);
        Label radiusLabel = glglBuilder.buildLabel(GLGL_RADIUS_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider radiusSlider = glglBuilder.buildSlider(GLGL_RADIUS_SLIDER, colorGradientPane, null, CLASS_GLGL_SLIDER, 0, 1, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        restructSlider(radiusSlider);
        Label cycleMethodLabel = glglBuilder.buildLabel(GLGL_CYCLE_METHOD_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ComboBox cycleMethodComboBox = glglBuilder.buildComboBox(GLGL_CYCLE_METHOD_COMBO_BOX, CYCLE_OPTIONS, DEFAULT_CYCLE, colorGradientPane, null, CLASS_GLGL_COMBOBOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label stop0Label = glglBuilder.buildLabel(GLGL_STOP_0_COLOR_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker stop0ColorPicker = glglBuilder.buildColorPicker(GLGL_STOP_0_COLOR_PICKER, colorGradientPane, null, CLASS_GLGL_COLOR_PICKER, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label stop1Label = glglBuilder.buildLabel(GLGL_STOP_1_COLOR_LABEL, colorGradientPane, null, CLASS_GLGL_LABEL, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        ColorPicker stop1ColorPicker = glglBuilder.buildColorPicker(GLGL_STOP_1_COLOR_PICKER, colorGradientPane, null, CLASS_GLGL_COLOR_PICKER, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

        registerSliderHandler(focusAngleSlider, "focusAngle");
        registerSliderHandler(focusDistanceSlider, "focusDistance");
        registerSliderHandler(centerXSlider, "centerX");
        registerSliderHandler(centerYSlider, "centerY");
        registerSliderHandler(radiusSlider, "colorRadius");

        registerChoiceHandler(cycleMethodComboBox, "cycleMethod");
        registerColorPickerHandler(stop0ColorPicker, "stop0");
        registerColorPickerHandler(stop1ColorPicker, "stop1");

        // hashStringToNode(colorGradientPane); 
    }

    public void restructSlider(Slider s) {
        s.setBlockIncrement(.1);
        s.setMajorTickUnit(.1);
        s.setSnapToTicks(true);
    }

    public void registerSliderHandler(Slider slider, String control) {

//         slider.addEventHandler(MouseEvent.MOUSE_DRAGGED, f -> {
        //STORE VALUE BEFORE MANIPULATED
//                goLoData data = (goLoData)app.getDataComponent(); 
//                goLoItem item = data.getSelectedItem(); 
//                switch(control){
//                   case "borderThickness": OGvalue = item.getGoLoBorder().getBorderThickness(); break; 
//                   case "borderRadius": OGvalue =item.getGoLoBorder().getBorderRadius(); break; 
//                   case "focusAngle": OGvalue =item.getGoLoRadGrad().getFocusAngle(); break; 
//                   case "focusDistance":OGvalue = item.getGoLoRadGrad().getFocusDistance(); break; 
//                   case "centerX": OGvalue =item.getGoLoRadGrad().getCenterX(); break; 
//                   case "centerY": OGvalue =item.getGoLoRadGrad().getCenterY(); break; 
//                   case "colorRadius": OGvalue =item.getGoLoRadGrad().getColorRadius(); break; 
//                }
//                
//                itemsController.processSliderEvent(slider, (int)slider.getValue(), control);
//                f.consume();
//        });
        slider.addEventHandler(MouseEvent.MOUSE_RELEASED, g -> {
            goLoData data = (goLoData) app.getDataComponent();
            goLoItem item = data.getSelectedItem();
            switch (control) {
                case "borderThickness":
                    OGvalue = item.getGoLoBorder().getBorderThickness();
                    break;
                case "borderRadius":
                    OGvalue = item.getGoLoBorder().getBorderRadius();
                    break;
                case "focusAngle":
                    OGvalue = item.getGoLoRadGrad().getFocusAngle();
                    break;
                case "focusDistance":
                    OGvalue = item.getGoLoRadGrad().getFocusDistance();
                    break;
                case "centerX":
                    OGvalue = item.getGoLoRadGrad().getCenterX();
                    break;
                case "centerY":
                    OGvalue = item.getGoLoRadGrad().getCenterY();
                    break;
                case "colorRadius":
                    OGvalue = item.getGoLoRadGrad().getColorRadius();
                    break;
            }
            itemsController.processSliderEventTransaction(slider, slider.getValue(), control);
            g.consume();
        });

    }

    public double getOGvalue() {
        return OGvalue;
    }

    public void registerColorPickerHandler(ColorPicker colorPicker, String control) {
        colorPicker.setOnHidden(e -> {
            itemsController.processChoiceEvent(colorPicker, colorPicker.getValue().toString(), control);
        });
    }

    public void registerChoiceHandler(ComboBox comboBox, String control) {
        comboBox.setOnHidden(e -> {
            itemsController.processChoiceEvent(comboBox, comboBox.getValue().toString(), control);
        });
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(GLGL_FOOLPROOF_SETTINGS,
                new goLoFoolproof((goLogoLoApp) app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
