/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import glgl.data.goLoData;
import static glgl.goLoPropertyType.GLGL_BORDER_STROKE_PANE;
import static glgl.goLoPropertyType.GLGL_COLOR_GRADIENT_PANE;
import static glgl.goLoPropertyType.GLGL_EDIT_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_FONT_PANE;
import static glgl.goLoPropertyType.GLGL_MOVE_DOWN_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_MOVE_UP_ITEM_BUTTON;
import static glgl.goLoPropertyType.GLGL_REMOVE_ITEM_BUTTON;
import glgl.goLogoLoApp;

/**
 *
 * @author musta
 */
public class goLoFoolproof implements FoolproofDesign {

    goLogoLoApp app;

    public goLoFoolproof(goLogoLoApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {

        AppGUIModule gui = app.getGUIModule();

        goLoData data = (goLoData) app.getDataComponent();
        boolean itemIsSelected = data.isItemSelected();
        gui.getGUINode(GLGL_REMOVE_ITEM_BUTTON).setDisable(!(itemIsSelected));
        gui.getGUINode(GLGL_MOVE_UP_ITEM_BUTTON).setDisable((!(itemIsSelected)) || data.isSelectedAtTop());
        gui.getGUINode(GLGL_MOVE_DOWN_ITEM_BUTTON).setDisable((!(itemIsSelected)) || data.isSelectedAtBottom());
        gui.getGUINode(GLGL_EDIT_ITEM_BUTTON).setDisable(!(itemIsSelected));
        gui.getGUINode(GLGL_FONT_PANE).setDisable(!(itemIsSelected && data.isTextItemSelected()));
        gui.getGUINode(GLGL_BORDER_STROKE_PANE).setDisable(!itemIsSelected || data.isTextItemSelected());
        gui.getGUINode(GLGL_COLOR_GRADIENT_PANE).setDisable(!itemIsSelected || data.isTextItemSelected());

        //DONT FORGET THE NAV TOOLBAR BUTTONS NEED TO HAVE FOOLPROOF_NESS HERE !!!
    }
}
