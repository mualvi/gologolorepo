/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.workspace;

import glgl.data.components.goLoComponentMethods;
import glgl.data.goLoData;
import glgl.data.goLoItem;
import static glgl.goLoPropertyType.GLGL_ITEMS_TABLE_VIEW;
import glgl.goLogoLoApp;
import glgl.transactions.TranslateItem_Transaction;
import java.util.HashMap;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.effect.Lighting;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author musta
 */
public class goLoCenterPane {

    public Pane center;
    public goLogoLoApp app;

    double startX, startY, endX, endY, height, width; 
    goLoItem selectedListItem;
    public HashMap<Integer, Integer> scenePositions = new HashMap<Integer, Integer>(); //Hashcode to positionOnPane

    //TableView.TableViewSelectionModel itemsSelectionModel; 
    //This is if you need it as a field.
    //Right now it is just returned as local, bc it is used twice!
    public goLoCenterPane(goLogoLoApp app) {
        center = new Pane();
        //center.toBack();
        this.app = app;

        TableView tableView = (TableView) app.getGUIModule().getGUINode(GLGL_ITEMS_TABLE_VIEW);
        TableView.TableViewSelectionModel itemsSelectionModel = tableView.getSelectionModel();

        center.setOnMouseClicked(e -> {
            itemsSelectionModel.clearSelection();
            processHighlight();
            app.getFoolproofModule().updateAll();

        });
        
    }

    public HashMap getScenePositions() {
        return scenePositions;
    }

    public Pane getCenter() {
//       Pane newnew = new Pane(); 
//       newnew.getChildren().add(createZoomPane());
//       return newnew; 
       return center;
    }

    public void initComponent(goLoItem itemContainer) {
        Node item = itemContainer.getComponent();
        itemContainer.setXY(itemContainer.getXPos(), itemContainer.getYPos());
        scenePositions.put(item.hashCode(), center.getChildren().indexOf(item));
        
        selectNodeHandler(itemContainer);
        makeDraggable(itemContainer);

    }

    public void addComponent(goLoItem itemContainer) {
        Node item = itemContainer.getComponent();
        item.setClip(new BorderPane().getCenter());
        center.getChildren().add(item);
        
        initComponent(itemContainer);

    }

    public void addComponentAt(int itemIndex, goLoItem itemContainer) {
        Node item = itemContainer.getComponent();
        item.setClip(new BorderPane().getCenter());
        center.getChildren().add(itemIndex, item);

        initComponent(itemContainer);
    }

    public void removeComponent(goLoItem itemContainer) {
        Node item = itemContainer.getComponent();

        center.getChildren().remove(item);
        scenePositions.remove(item.getId());

    }

    public void swapComponent(int beforeMoveIndex, int afterMoveIndex) {

        //Alternate imp, was throwing duplicateChildException from Parent class??? 
        //So I had to hard code the formula
        //   Collections.swap(center.getChildren(), beforeMoveIndex, afterMoveIndex); 
        List<Node> swap = center.getChildren();
        Node removed = swap.remove(beforeMoveIndex);
        // removed.setClip(new GridPane());
        swap.add(afterMoveIndex, removed);

    }

    public void processHighlight() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem item = data.getSelectedItem();
            if (selectedListItem != null) //First time it should skip this, bc it will be null 
            {
                selectedListItem.getComponent().setEffect(null);
            }
            selectedListItem = item;
            selectedListItem.getComponent().setEffect(new Lighting());
        } else if (selectedListItem != null) {
            selectedListItem.getComponent().setEffect(null);
        }
    }

    void selectNodeHandler(goLoItem item) {
        Node node = item.getComponent();
        TableView tableView = (TableView) app.getGUIModule().getGUINode(GLGL_ITEMS_TABLE_VIEW);
        TableView.TableViewSelectionModel itemsSelectionModel = tableView.getSelectionModel();

        node.addEventFilter(
                MouseEvent.MOUSE_CLICKED, e -> {
                    itemsSelectionModel.clearAndSelect(scenePositions.get(node.hashCode()));
                    //Add Styling to the node 
                    processHighlight();
                    app.getFoolproofModule().updateAll();
                    e.consume();
                });
    }

    public void makeDraggable(goLoItem item) {

        goLoComponentMethods var = item;
        Node node = item.getComponent();

//        node.addEventFilter(
//                MouseEvent.ANY,e->{
//                // disable mouse events for all children
//                e.consume();
//        });
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, e -> {
            startX = item.getXPos();
            startY = item.getYPos();
        });

        node.addEventFilter(MouseEvent.MOUSE_DRAGGED, e -> {
            var.initDrag(e);
        });

        node.addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {

            endX = e.getX();
            endY = e.getY();

            TranslateItem_Transaction transaction = new TranslateItem_Transaction(var, startX, startY, endX, endY);
            app.processTransaction(transaction);
            item.setXPos((int) endX);
            item.setYPos((int) endY);

        });

        return;

    }
    
//    private Parent createZoomPane() {
//    final double SCALE_DELTA = 1.1;
//    final StackPane zoomPane = new StackPane();
//
//    zoomPane.getChildren().add(center);
//
//    final ScrollPane scroller = new ScrollPane();
//    final Group scrollContent = new Group(zoomPane);
//    scroller.setContent(scrollContent);
//
//    scroller.viewportBoundsProperty().addListener(new ChangeListener<Bounds>() {
//      @Override
//      public void changed(ObservableValue<? extends Bounds> observable,
//          Bounds oldValue, Bounds newValue) {
//        zoomPane.setMinSize(newValue.getWidth(), newValue.getHeight());
//      }
//    });
//    
//    scroller.setPrefViewportWidth(center.getWidth());
//    scroller.setPrefViewportHeight(center.getHeight());
//
//    zoomPane.setOnScroll(new EventHandler<ScrollEvent>() {
//      @Override
//      public void handle(ScrollEvent event) {
//        event.consume();
//
//        if (event.getDeltaY() == 0) {
//          return;
//        }
//
//        double scaleFactor = (event.getDeltaY() > 0) ? SCALE_DELTA
//            : 1 / SCALE_DELTA;
//
//        // amount of scrolling in each direction in scrollContent coordinate
//        // units
//        Point2D scrollOffset = figureScrollOffset(scrollContent, scroller);
//
//        center.setScaleX(center.getScaleX() * scaleFactor);
//        center.setScaleY(center.getScaleY() * scaleFactor);
//
//        // move viewport so that old center remains in the center after the
//        // scaling
//        repositionScroller(scrollContent, scroller, scaleFactor, scrollOffset);
//
//      }
//    });
//
//    // Panning via drag....
//    final ObjectProperty<Point2D> lastMouseCoordinates = new SimpleObjectProperty<Point2D>();
//    scrollContent.setOnMousePressed(new EventHandler<MouseEvent>() {
//      @Override
//      public void handle(MouseEvent event) {
//        lastMouseCoordinates.set(new Point2D(event.getX(), event.getY()));
//      }
//    });
//
//    scrollContent.setOnMouseDragged(new EventHandler<MouseEvent>() {
//      @Override
//      public void handle(MouseEvent event) {
//        double deltaX = event.getX() - lastMouseCoordinates.get().getX();
//        double extraWidth = scrollContent.getLayoutBounds().getWidth() - scroller.getViewportBounds().getWidth();
//        double deltaH = deltaX * (scroller.getHmax() - scroller.getHmin()) / extraWidth;
//        double desiredH = scroller.getHvalue() - deltaH;
//        scroller.setHvalue(Math.max(0, Math.min(scroller.getHmax(), desiredH)));
//
//        double deltaY = event.getY() - lastMouseCoordinates.get().getY();
//        double extraHeight = scrollContent.getLayoutBounds().getHeight() - scroller.getViewportBounds().getHeight();
//        double deltaV = deltaY * (scroller.getHmax() - scroller.getHmin()) / extraHeight;
//        double desiredV = scroller.getVvalue() - deltaV;
//        scroller.setVvalue(Math.max(0, Math.min(scroller.getVmax(), desiredV)));
//      }
//    });
//
//    return scroller;
//  }
//
//  private Point2D figureScrollOffset(Node scrollContent, ScrollPane scroller) {
//    double extraWidth = scrollContent.getLayoutBounds().getWidth() - scroller.getViewportBounds().getWidth();
//    double hScrollProportion = (scroller.getHvalue() - scroller.getHmin()) / (scroller.getHmax() - scroller.getHmin());
//    double scrollXOffset = hScrollProportion * Math.max(0, extraWidth);
//    double extraHeight = scrollContent.getLayoutBounds().getHeight() - scroller.getViewportBounds().getHeight();
//    double vScrollProportion = (scroller.getVvalue() - scroller.getVmin()) / (scroller.getVmax() - scroller.getVmin());
//    double scrollYOffset = vScrollProportion * Math.max(0, extraHeight);
//    return new Point2D(scrollXOffset, scrollYOffset);
//  }
//
//  private void repositionScroller(Node scrollContent, ScrollPane scroller, double scaleFactor, Point2D scrollOffset) {
//    double scrollXOffset = scrollOffset.getX();
//    double scrollYOffset = scrollOffset.getY();
//    double extraWidth = scrollContent.getLayoutBounds().getWidth() - scroller.getViewportBounds().getWidth();
//    if (extraWidth > 0) {
//      double halfWidth = scroller.getViewportBounds().getWidth() / 2 ;
//      double newScrollXOffset = (scaleFactor - 1) *  halfWidth + scaleFactor * scrollXOffset;
//      scroller.setHvalue(scroller.getHmin() + newScrollXOffset * (scroller.getHmax() - scroller.getHmin()) / extraWidth);
//    } else {
//      scroller.setHvalue(scroller.getHmin());
//    }
//    double extraHeight = scrollContent.getLayoutBounds().getHeight() - scroller.getViewportBounds().getHeight();
//    if (extraHeight > 0) {
//      double halfHeight = scroller.getViewportBounds().getHeight() / 2 ;
//      double newScrollYOffset = (scaleFactor - 1) * halfHeight + scaleFactor * scrollYOffset;
//      scroller.setVvalue(scroller.getVmin() + newScrollYOffset * (scroller.getVmax() - scroller.getVmin()) / extraHeight);
//    } else {
//      scroller.setVvalue(scroller.getVmin());
//    }
//  }

}
