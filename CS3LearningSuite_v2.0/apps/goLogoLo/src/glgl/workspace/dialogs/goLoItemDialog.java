/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.workspace.dialogs;

import static djf.AppTemplate.PATH_WORK;
import djf.modules.AppLanguageModule;
import djf.ui.dialogs.AppDialogsFacade;
import glgl.data.components.goLoImageItem;
import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.data.components.goLoTextItem;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_ADD_HEADER_TEXT;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_CANCEL_BUTTON;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_EDIT_HEADER_TEXT;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_EDIT_NAME_HEADER_TEXT;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_TEXT_PROMPT;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_HEADER;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_OK_BUTTON;
import static glgl.goLoPropertyType.GLGL_ITEM_DIALOG_RESIZE_HEADER_TEXT;
import static glgl.goLoPropertyType.GLGL_ITEM_INVALID_MESSAGE;
import static glgl.goLoPropertyType.GLGL_ITEM_INVALID_TITLE;
import glgl.goLogoLoApp;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_DIALOG_BUTTON;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_DIALOG_GRID;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_DIALOG_HEADER;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_DIALOG_PANE;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_DIALOG_PROMPT;
import static glgl.workspace.style.GLStyle.CLASS_GLGL_DIALOG_TEXT_FIELD;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;

/**
 *
 * @author musta
 */
public class goLoItemDialog extends Stage {

    goLogoLoApp app;
    GridPane gridPane;

    Label headerLabel = new Label();
    Label textLabel = new Label();
    TextField textTextField = new TextField();
    TextField text2TextField = new TextField();

    HBox okCancelPane = new HBox();
    Button okButton = new Button();
    Button cancelButton = new Button();

    Pane paneToEdit;
    goLoItem itemToEdit;
    goLoItem newItem;
    String editTextItemString;
    goLoItem resizeItem;
    String newName;
    boolean editingTextItem;
    boolean editingName;
    boolean editingSize;

    BufferedImage img;

    private double height;
    private double width;


    public goLoItemDialog(goLogoLoApp initApp) {
        app = initApp;

        // EVERYTHING GOES IN HERE
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_GLGL_DIALOG_GRID);
        initDialog();

        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);

        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_GLGL_DIALOG_GRID);

        // MAKE IT MODAL
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }

    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();

        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled) node).textProperty());
            ((Labeled) node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled) node).tooltipProperty().get().textProperty());
        }

        // PUT IT IN THE UI
        if (col >= 0) {
            gridPane.add(node, col, row, colSpan, rowSpan);
        }

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }

    protected void initDialog() {
        // THE NODES ABOVE GO DIRECTLY INSIDE THE GRID
        initGridNode(headerLabel, GLGL_ITEM_DIALOG_HEADER, CLASS_GLGL_DIALOG_HEADER, 0, 0, 3, 1, true);

        initGridNode(textLabel, GLGL_ITEM_DIALOG_TEXT_PROMPT, CLASS_GLGL_DIALOG_PROMPT, 0, 1, 1, 1, true);
        initGridNode(textTextField, null, CLASS_GLGL_DIALOG_TEXT_FIELD, 1, 1, 1, 1, false);
        text2TextField.setEditable(false);
        text2TextField.setVisible(false);
        initGridNode(text2TextField, null, CLASS_GLGL_DIALOG_TEXT_FIELD, 1, 2, 1, 1, false);

        initGridNode(okCancelPane, null, CLASS_GLGL_DIALOG_PANE, 0, 3, 3, 1, false);

        okButton = new Button();
        cancelButton = new Button();
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_OK_BUTTON, okButton);
        app.getGUIModule().addGUINode(GLGL_ITEM_DIALOG_CANCEL_BUTTON, cancelButton);
        okButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_GLGL_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);

        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_OK_BUTTON + "_TEXT", okButton.textProperty());
        languageSettings.addLabeledControlProperty(GLGL_ITEM_DIALOG_CANCEL_BUTTON + "_TEXT", cancelButton.textProperty());

        // AND SETUP THE EVENT HANDLERS
        okButton.setOnAction(e -> {
            processOKButton();
        });
        cancelButton.setOnAction(e -> {
            newItem = null;
            this.hide();
        });
    }

    private void processOKButton() {
        // GET THE SETTINGS
        String inputText = textTextField.getText();

        // IF WE ARE EDITING
        goLoData data = (goLoData) app.getDataComponent();
        //EDITING THE TEXTCOMPONENT
        if (editingTextItem) {
            if (data.isValidNewTextItem(inputText)) {
                 editTextItemString = inputText;
            } else {
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), GLGL_ITEM_INVALID_TITLE, GLGL_ITEM_INVALID_MESSAGE);
            }

        } //EDITING GOLOITEM NAME
        else if (editingName) {
            if (data.isValidNewTextItem(inputText)) {
                newName = inputText;
            } else {
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), GLGL_ITEM_INVALID_TITLE, GLGL_ITEM_INVALID_MESSAGE);
            }

        }// EDITING LOGO SIZE 
        else if (editingSize) {
            if (data.isValidNewTextItem(inputText)) {
                processResizeWork();
            } else {
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), GLGL_ITEM_INVALID_TITLE, GLGL_ITEM_INVALID_MESSAGE);
            }

        } //MAKING A NEW TEXT ITEM
        else {
            if (data.isValidNewTextItem(inputText)) {
                this.makeNewTextItem();
            } else {
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), GLGL_ITEM_INVALID_TITLE, GLGL_ITEM_INVALID_MESSAGE);
            }
        }

        // CLOSE THE DIALOG
        this.hide();
    }

    private void processResizeWork() {
        height = Double.parseDouble(textTextField.getText());
        width = Double.parseDouble(text2TextField.getText());

        if (height < 0 || width < 0) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), GLGL_ITEM_INVALID_TITLE, GLGL_ITEM_INVALID_MESSAGE);
        } else if (height > 1000 || width > 1000) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), GLGL_ITEM_INVALID_TITLE, GLGL_ITEM_INVALID_MESSAGE);
        }

    }

    private void makeNewTextItem() {
        String text = textTextField.getText();

        newItem = new goLoTextItem(text);
        this.hide();
    }

    public void showAddTextDialog() {
        textTextField.setPromptText("");
        text2TextField.setEditable(false);
        text2TextField.setVisible(false);
        // USE THE TEXT IN THE HEADER FOR ADD
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_ADD_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);

        // USE THE TEXT IN THE HEADER FOR ADD
        textTextField.setText("");

        // WE ARE ADDING A NEW ONE, NOT EDITING
        editingTextItem = false;
        editingName = false; 

        // AND OPEN THE DIALOG
        showAndWait();

    }

    public void showEditTextDialog(goLoItem initItemToEdit) {
        textTextField.setPromptText("Enter New Text Value");
        text2TextField.setEditable(false);
        text2TextField.setVisible(false);
        // WE'LL NEED THIS FOR VALIDATION
        itemToEdit = initItemToEdit;

        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_EDIT_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);

        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        editingTextItem = true;
        editingName = false; 
        editingSize = false; 

        // USE THE TEXT IN THE HEADER FOR EDIT
        textTextField.setText(((goLoTextItem) itemToEdit).toString());

        // AND OPEN THE DIALOG
        showAndWait();
    }

    public void showEditNameDialog(goLoItem initItemToEdit) {
        textTextField.setPromptText("Enter New Component Name");
        text2TextField.setEditable(false);
        text2TextField.setVisible(false);
        // WE'LL NEED THIS FOR VALIDATION
        itemToEdit = initItemToEdit;

        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_EDIT_NAME_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);

        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        editingTextItem = false;
        editingName = true;
        editingSize = false;

        // USE THE TEXT IN THE HEADER FOR EDIT
        textTextField.setText((itemToEdit).getName().toString());

        // AND OPEN THE DIALOG
        showAndWait();
    }

    public void showResizeDialog(Pane p) {
        paneToEdit = p;
        textTextField.setPromptText("Enter Height");
        text2TextField.setEditable(true);
        text2TextField.setVisible(true);
        text2TextField.setPromptText("Enter Width");

        // WE'LL NEED THIS FOR VALIDATION
        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(GLGL_ITEM_DIALOG_RESIZE_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);

        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        editingTextItem = false;
        editingName = false;
        editingSize = true;

        height = p.getScaleY();
        width = p.getScaleX();

        // USE THE TEXT IN THE HEADER FOR EDIT
        textTextField.setText(String.valueOf(height));
        text2TextField.setText(String.valueOf(width));

        // AND OPEN THE DIALOG
        showAndWait();

    }

    public void showAddImageDialog() {
        FileChooser fileChooser = new FileChooser();
        goLogoLoApp goLoApp = (goLogoLoApp) app;

        fileChooser.setInitialDirectory(new File(PATH_WORK));
        fileChooser.setTitle("Select Image");

        //Set extension filter
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG Files", "*.png"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JPG Files", "*.jpg"));

        //Prompt user to select a file
        File file = fileChooser.showOpenDialog(null);

        if (file != null) {
            try {
                //Pad the capture area
                img = ImageIO.read(file);
                //Write the snapshot to the chosen file
            } catch (IOException ex) {
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), GLGL_ITEM_INVALID_TITLE, GLGL_ITEM_INVALID_MESSAGE);
            }

        }
    }

    public goLoItem getNewItem() {
        return newItem;
    }

    public String getEditTextItemString() {
        return editTextItemString;
    }

    public String getNewName() {
        return newName;
    }

    public double getPaneHeight() {
        return height;
    }

    public double getPaneWidth() {
        return width;
    }

    public BufferedImage getImage() {
        return (img);
    }

}
