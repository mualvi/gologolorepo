/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.clipboard;

import djf.AppTemplate;
import djf.components.AppClipboardComponent;
import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import glgl.transactions.CutItems_Transaction;
import glgl.transactions.PasteItems_Transaction;
import java.util.ArrayList;

/**
 *
 * @author musta
 */
public class goLoClipboard implements AppClipboardComponent {

    goLogoLoApp app;
    goLoItem clipboardCutItem;
    goLoItem clipboardCopiedItem;

    public goLoClipboard(goLogoLoApp initApp) {
        app = initApp;
        clipboardCutItem = null;
        clipboardCopiedItem = null;
    }

    @Override
    public void cut() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            clipboardCutItem = data.getSelectedItem();
            clipboardCopiedItem = null;
            CutItems_Transaction transaction = new CutItems_Transaction((goLogoLoApp) app, clipboardCutItem);
            app.processTransaction(transaction);
        }
    }

    private void copyToCutClipboard(goLoItem itemToCopy) {
        clipboardCutItem = copyItem(itemToCopy);
        clipboardCopiedItem = null;
        app.getFoolproofModule().updateAll();
    }

    @Override
    public void copy() {
        goLoData data = (goLoData) app.getDataComponent();
        if (data.isItemSelected()) {
            goLoItem tempItem = data.getSelectedItem();
            copyToCopiedClipboard(tempItem);
        }
    }

    private void copyToCopiedClipboard(goLoItem itemToCopy) {
        clipboardCutItem = null;
        clipboardCopiedItem = copyItem(itemToCopy);
        app.getFoolproofModule().updateAll();
    }

    private goLoItem copyItem(goLoItem itemToCopy) {
        goLoItem copiedItem = (goLoItem) itemToCopy.clone();        

        return copiedItem;
    }

    @Override
    public void paste() {
        if (hasSomethingToPaste()) {
            if ((clipboardCutItem != null)) {
                PasteItems_Transaction transaction = new PasteItems_Transaction((goLogoLoApp) app, clipboardCutItem);
                app.processTransaction(transaction);

                // NOW WE HAVE TO RE-COPY THE CUT ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                copyToCutClipboard(clipboardCutItem);
            } else if ((clipboardCopiedItem != null)) {
                PasteItems_Transaction transaction = new PasteItems_Transaction((goLogoLoApp) app, clipboardCopiedItem);
                app.processTransaction(transaction);

                // NOW WE HAVE TO RE-COPY THE COPIED ITEMS TO MAKE
                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
                copyToCopiedClipboard(clipboardCopiedItem);
            }
        }
    }

    @Override
    public boolean hasSomethingToCut() {
        return ((goLoData) app.getDataComponent()).isItemSelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((goLoData) app.getDataComponent()).isItemSelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        if ((clipboardCutItem != null)) {
            return true;
        } else if ((clipboardCopiedItem != null)) {
            return true;
        } else {
            return false;
        }
    }

}
