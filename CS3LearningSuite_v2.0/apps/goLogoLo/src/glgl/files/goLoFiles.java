/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.files;

import djf.AppTemplate;
import static djf.AppTemplate.PATH_WORK;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import glgl.data.components.goLoCircleItem;
import glgl.data.components.goLoRectangleItem;
import glgl.data.components.goLoTextItem;
import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.data.properties.goLoBorderStroke;
import glgl.data.properties.goLoFont;
import glgl.data.properties.goLoRadialGradient;
import glgl.goLogoLoApp;
import glgl.workspace.goLoWorkspace;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author musta
 */
public class goLoFiles implements AppFileComponent {

    static final String JSON_ORDER = "order";
    static final String JSON_NAME = "name";
    static final String JSON_TYPE = "type";
    static final String JSON_ID = "id";
    static final String JSON_ITEMS = "items";
    static final String JSON_XPOS = "xPos";
    static final String JSON_YPOS = "yPos";
    static final String JSON_FONT = "font";
    static final String JSON_FONT_SIZE = "fontSize";
    static final String JSON_BOLDED = "bolded";
    static final String JSON_ITALICIZED = "italicized";
    static final String JSON_UNDERLINED = "underlined";
    static final String JSON_BORDER_THICKNESS = "borderThickness";
    static final String JSON_BORDER_COLOR = "borderColor";
    static final String JSON_BORDER_RADIUS = "borderRadius";

    static final String JSON_FOCUS_ANGLE = "focusAngle";
    static final String JSON_FOCUS_DISTANCE = "focusDistance";
    static final String JSON_CENTER_X = "centerX";
    static final String JSON_CENTER_Y = "centerY";
    static final String JSON_COLOR_RADIUS = "colorRadius";
    static final String JSON_CYCLE = "cycle";
    static final String JSON_STOP_0 = "stop0";
    static final String JSON_STOP_1 = "stop1";

    public goLoFiles() {
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {

        // GET THE DATA
        goLoData goLoData = (goLoData) data;

        // NOW BUILD THE JSON ARRAY FOR THE LIST
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        Iterator<? super goLoItem> itemsIt = goLoData.itemsIterator();
        while (itemsIt.hasNext()) {
            goLoItem item = (goLoItem) itemsIt.next();
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_ORDER, item.getOrder())
                    .add(JSON_NAME, item.getName())
                    .add(JSON_TYPE, item.getType())
                    .add(JSON_XPOS, item.getXPos())
                    .add(JSON_YPOS, item.getYPos())
                    .add(JSON_FONT, item.getGoLoFont().getFont())
                    .add(JSON_FONT_SIZE, item.getGoLoFont().getFontSize())
                    .add(JSON_BOLDED, item.getGoLoFont().getBolded())
                    .add(JSON_ITALICIZED, item.getGoLoFont().getItalicized())
                    .add(JSON_UNDERLINED, item.getGoLoFont().getUnderlined())
                    .add(JSON_BORDER_THICKNESS, item.getGoLoBorder().getBorderThickness())
                    .add(JSON_BORDER_COLOR, item.getGoLoBorder().getBorderColor())
                    .add(JSON_BORDER_RADIUS, item.getGoLoBorder().getBorderRadius())
                    .add(JSON_FOCUS_ANGLE, item.getGoLoRadGrad().getFocusAngle())
                    .add(JSON_FOCUS_DISTANCE, item.getGoLoRadGrad().getFocusDistance())
                    .add(JSON_CENTER_X, item.getGoLoRadGrad().getCenterX())
                    .add(JSON_CENTER_Y, item.getGoLoRadGrad().getCenterY())
                    .add(JSON_COLOR_RADIUS, item.getGoLoRadGrad().getColorRadius())
                    .add(JSON_CYCLE, item.getGoLoRadGrad().getCycle())
                    .add(JSON_STOP_0, item.getGoLoRadGrad().getStop0())
                    .add(JSON_STOP_1, item.getGoLoRadGrad().getStop1())
                    .build();

            arrayBuilder.add(itemJson);
        }
        JsonArray itemsArray = arrayBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject goLoDataJSO = Json.createObjectBuilder()
                .add(JSON_ITEMS, itemsArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(goLoDataJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(goLoDataJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {

        // CLEAR THE OLD DATA OUT
        goLoData goLoData = (goLoData) data;
        goLoData.reset();

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // AND NOW LOAD ALL THE ITEMS
        JsonArray jsonItemArray = json.getJsonArray(JSON_ITEMS);
        for (int i = 0; i < jsonItemArray.size(); i++) {
            JsonObject jsonItem = jsonItemArray.getJsonObject(i);
            goLoItem item = loadItem(jsonItem);
            goLoData.addItem(item);
        }
    }

    public goLoItem loadItem(JsonObject jsonItem) {
        // GET THE DATA
        goLoItem item = null;

        JsonNumber temp;
        int order = jsonItem.getInt(JSON_ORDER);
        String name = jsonItem.getString(JSON_NAME);
        String type = jsonItem.getString(JSON_TYPE);
        int xPos = jsonItem.getInt(JSON_XPOS);
        int yPos = jsonItem.getInt(JSON_YPOS);

        String font = jsonItem.getString(JSON_FONT);
        int fontSize = jsonItem.getInt(JSON_FONT_SIZE);
        boolean bolded = jsonItem.getBoolean(JSON_BOLDED);
        boolean italicized = jsonItem.getBoolean(JSON_ITALICIZED);
        boolean underlined = jsonItem.getBoolean(JSON_UNDERLINED);

        temp = jsonItem.getJsonNumber(JSON_BORDER_THICKNESS);
        double borderThickness = temp.doubleValue();
        String borderColor = jsonItem.getString(JSON_BORDER_COLOR);
        temp = jsonItem.getJsonNumber(JSON_BORDER_RADIUS);
        double borderRadius = temp.doubleValue();

        int focusAngle = jsonItem.getInt(JSON_FOCUS_ANGLE);

        temp = jsonItem.getJsonNumber(JSON_FOCUS_DISTANCE);
        double focusDistance = temp.doubleValue();
        temp = jsonItem.getJsonNumber(JSON_CENTER_X);
        double centerX = temp.doubleValue();
        temp = jsonItem.getJsonNumber(JSON_CENTER_Y);
        double centerY = temp.doubleValue();
        temp = jsonItem.getJsonNumber(JSON_COLOR_RADIUS);
        double colorRadius = temp.doubleValue();
        String cycle = jsonItem.getString(JSON_CYCLE);
        String stop0 = jsonItem.getString(JSON_STOP_0);
        String stop1 = jsonItem.getString(JSON_STOP_1);

        goLoRadialGradient radialGrad
                = new goLoRadialGradient()
                        .setFocusAngle(focusAngle)
                        .setFocusDistance(focusDistance)
                        .setCenterX(centerX)
                        .setCenterY(centerY)
                        .setColorRadius(colorRadius)
                        .setCycle(cycle)
                        .setStop0(stop0)
                        .setStop1(stop1);

        goLoBorderStroke border = new goLoBorderStroke()
                .setBorderThickness(borderThickness)
                .setBorderColor(borderColor)
                .setBorderRadius(borderRadius);

        goLoFont componentFont = new goLoFont()
                .setFont(font)
                .setFontSize(fontSize)
                .setBolded(bolded)
                .setItalicized(italicized)
                .setUnderlined(underlined);

        if (type.equals("goLoTextItem")) {
            item = new goLoTextItem(order, name, type, xPos, yPos, radialGrad, border);
            item.setGoLoFont(componentFont);
        } else if (type.equals("goLoRectangleItem")) {
            item = new goLoRectangleItem(order, name, type, xPos, yPos, radialGrad, border);
        } else if (type.equals("goLoCircleItem")) {
            item = new goLoCircleItem(order, name, type, xPos, yPos, radialGrad, border);
        }

        // ALL DONE, RETURN IT
        return item;

    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void exportData(AppTemplate app, AppDataComponent data) throws IOException {
        FileChooser fileChooser = new FileChooser();
        goLogoLoApp goLoApp = (goLogoLoApp) app;

        fileChooser.setInitialDirectory(new File(PATH_WORK));
        fileChooser.setTitle("Export Logo");

        //Set extension filter
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG Files", "*.png"));

        //Prompt user to select a file
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                //Pad the capture area
                Node center = ((goLoWorkspace) (goLoApp.getWorkspaceComponent())).getCenter().getCenter();
                WritableImage writableImage = new WritableImage(1500, 1200);
                center.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                //Write the snapshot to the chosen file
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            /*
        // PROMPT THE USER FOR A FILE NAME
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        fc.setTitle(props.getProperty(saveTitleProp));
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC), props.getProperty(WORK_FILE_EXT))
        );
        File selectedFile = fc.showSaveDialog(window);
        return selectedFile;
             */
        }
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
