/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data;

import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import glgl.data.components.goLoTextItem;
import glgl.data.properties.goLoFont;
import static glgl.goLoPropertyType.GLGL_ITEMS_TABLE_VIEW;
import glgl.goLogoLoApp;
import glgl.workspace.goLoWorkspace;
import glgl.workspace.goLoCenterPane;
import java.util.Collections;
import java.util.Iterator;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.Slider;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;

/**
 *
 * @author musta
 */
public class goLoData implements AppDataComponent {

    goLogoLoApp app;
    ObservableList<goLoItem> items;
    TableViewSelectionModel itemsSelectionModel;
    goLoCenterPane center;

    //StringProperty ownerProperty;
    // StringProperty listNameProperty;
    public goLoData(goLogoLoApp initApp) {
        app = initApp;

        // GET ALL THE THINGS WE'LL NEED TO MANIUPLATE THE TABLE AND PANE
        TableView tableView = (TableView) app.getGUIModule().getGUINode(GLGL_ITEMS_TABLE_VIEW);
        items = tableView.getItems();
        itemsSelectionModel = tableView.getSelectionModel();
        center = ((goLoWorkspace) (app.getWorkspaceComponent())).getCenter();

    }

    public Iterator<goLoItem> itemsIterator() {
        return this.items.iterator();
    }

    public TableViewSelectionModel getSelectionModel() {
        return itemsSelectionModel;
    }

    public int getNumItems() {
        return items.size();

    }

    public int getPosition(goLoItem x) {
        return items.indexOf(x);
    }

    public void addItem(goLoItem itemToAdd) {
        items.add(itemToAdd);
        itemToAdd.setName(itemToAdd.toString());
        itemToAdd.setType(itemToAdd.getClass().getSimpleName());
        center.addComponent(itemToAdd);
        renumberTable();
    }

    public void addItemAt(goLoItem itemToAdd, int itemIndex) {
        items.add(itemIndex, itemToAdd);
        itemToAdd.setName(itemToAdd.toString());
        itemToAdd.setType(itemToAdd.getClass().getSimpleName());
        center.addComponentAt(itemIndex, itemToAdd);
        renumberTable();

    }

    public void removeItem(goLoItem itemToRemove) {
        items.remove(itemToRemove);
        center.removeComponent(itemToRemove);
        renumberTable();
    }

    public int removeItemGetIndex(goLoItem itemToRemove) {
        int index = items.indexOf(itemToRemove);
        items.remove(itemToRemove);
        center.removeComponent(itemToRemove);
        renumberTable();

        return index;
    }

    public void editTextItem(String newText) {
        goLoTextItem textItem = (goLoTextItem)getSelectedItem(); 
        textItem.getComponent().setText(newText);
    }

    public void editItemName(goLoItem item, String newName) {
        item.setName(newName);
    }

    public void editSliderValue(goLoItem item, String controlType, double value, Slider slider) {
        switch (controlType) {
            case "borderThickness":
                item.getGoLoBorder().setBorderThickness(value);
                break;
            case "borderRadius":
                item.getGoLoBorder().setBorderRadius(value);
                break;
            case "focusAngle":
                item.getGoLoRadGrad().setFocusAngle(value);
                break;
            case "focusDistance":
                item.getGoLoRadGrad().setFocusDistance(value);
                break;
            case "centerX":
                item.getGoLoRadGrad().setCenterX(value);
                break;
            case "centerY":
                item.getGoLoRadGrad().setCenterY(value);
                break;
            case "colorRadius":
                item.getGoLoRadGrad().setColorRadius(value);
                break;
        }
        slider.setValue(value);
        item.initRadGrad();
        item.initBorder();
    }

    public void editChoiceValue(goLoItem item, String controlType, String value, ComboBoxBase box) {
        switch (controlType) {
            case "borderColor":
                item.getGoLoBorder().setBorderColor(value);
                box.setValue(Paint.valueOf(value));
                item.initRadGrad();
                item.initBorder();
                break;
            case "cycleMethod":
                item.getGoLoRadGrad().setCycle(value);
                box.setValue(value);
                item.initRadGrad();
                item.initBorder();
                break;
            case "stop0":
                item.getGoLoRadGrad().setStop0(value);
                box.setValue(Paint.valueOf(value));
                item.initRadGrad();
                item.initBorder();
                break;
            case "stop1":
                item.getGoLoRadGrad().setStop1(value);
                box.setValue(Paint.valueOf(value));
                item.initRadGrad();
                item.initBorder();
                break;
            case "fontStyle":
                item.getGoLoFont().setFont(value);
                item.initTextItem();
                break;
            case "fontSize":
                item.getGoLoFont().setFontSize(Integer.parseInt(value));
                item.initTextItem();
                break;
        }

    }

    public void handleBooleanButtons(goLoItem item, String controlSource) {
        switch (controlSource) {
            case "italic":
                item.getGoLoFont().setItalicized(!item.getGoLoFont().getItalicized());
                break;
            case "bold":
                item.getGoLoFont().setBolded(!item.getGoLoFont().getBolded());
                break;
            case "underline":
                item.getGoLoFont().setUnderlined(!item.getGoLoFont().getUnderlined());
                break;
        }

        item.initTextItem();
    }

    @Override
    public void reset() {
        AppGUIModule gui = app.getGUIModule();

        //REINIT PANES of WORK
        //  ((goLoWorkspace)(app.getWorkspaceComponent())).reset(); 
        // CLEAR OUT THE ITEMS FROM THE TABLE
        TableView tableView = (TableView) gui.getGUINode(GLGL_ITEMS_TABLE_VIEW);
        items = tableView.getItems();
        items.clear();
        center.center.getChildren().clear();

    }

    public boolean isItemSelected() {
        ObservableList<goLoItem> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() == 1);
    }

    public goLoItem getSelectedItem() {
        if (!isItemSelected()) {
            return null;
        }
        return getSelectedItems().get(0);
    }

    public ObservableList<goLoItem> getSelectedItems() {
        return (ObservableList<goLoItem>) this.itemsSelectionModel.getSelectedItems();
    }

    public boolean areItemsSelected() {
        ObservableList<goLoItem> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() > 1);
    }

    public void clearSelected() {
        this.itemsSelectionModel.clearSelection();
    }

    public boolean isValidNewTextItem(String textInput) {
        if (textInput.trim().length() == 0) {
            return false;
        }
        return true;
    }

    public void moveUpItem(goLoItem itemToMoveUp) {
        int indexOfOrigin = items.indexOf(itemToMoveUp);
        int indexOfDestination = indexOfOrigin - 1;
        Collections.swap(items, indexOfOrigin, indexOfDestination);
        renumberTable();

        center.swapComponent(indexOfOrigin, indexOfDestination);

    }

    public void moveDownItem(goLoItem itemToMoveDown) {
        int indexOfOrigin = items.indexOf(itemToMoveDown);
        int indexOfDestination = indexOfOrigin + 1;
        Collections.swap(items, indexOfOrigin, indexOfDestination);
        renumberTable();

        center.swapComponent(indexOfOrigin, indexOfDestination);

    }

    public void renumberTable() {
        for (goLoItem x : items) {
            x.setOrder(getPosition(x));
        }
    }

    public boolean isSelectedAtTop() {
        return (items.indexOf(this.getSelectedItem()) == 0);
    }

    public boolean isSelectedAtBottom() {
        return (items.indexOf(this.getSelectedItem()) == (this.getNumItems()) - 1);
    }

    public int getItemIndex(goLoItem item) {
        return items.indexOf(item);
    }

    public boolean isTextItemSelected() {
        goLoData data = (goLoData) app.getDataComponent();
        if (!data.isItemSelected()) {
            return false;
        }
        return data.getSelectedItem().getType().equals("goLoTextItem");
    }

    public void increaseFontSize(goLoItem item) {
        goLoFont go = item.getGoLoFont();
        go.setFontSize(go.getFontSize() + 10);
        item.initTextItem();
    }

    public void decreaseFontSize(goLoItem item) {
        goLoFont go = item.getGoLoFont();
        go.setFontSize(go.getFontSize() - 10);
        item.initTextItem();
    }

    public void resizePane(Pane p, double prefWidth, double prefHeight) {
        p.setScaleX(prefHeight);
        p.setScaleY(prefWidth);
    }
}
