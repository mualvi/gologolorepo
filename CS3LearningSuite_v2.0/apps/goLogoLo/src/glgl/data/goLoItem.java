/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data;

import glgl.data.components.goLoComponentMethods;
import glgl.data.properties.goLoBorderStroke;
import glgl.data.properties.goLoFont;
import glgl.data.properties.goLoRadialGradient;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author musta
 */
public abstract class goLoItem implements goLoComponentMethods {

    public static final int DEFAULT_ORDER = 0;
    public static final String DEFAULT_NAME = "Item";
    public static final String DEFAULT_TYPE = "Component";
    public static final int DEFAULT_X_POSITION = 100;
    public static final int DEFAULT_Y_POSITION = 100;

    private goLoBorderStroke itemBorder;
    private goLoRadialGradient itemRadialGradient;
    private goLoFont itemFont;

//TableView vars
    final IntegerProperty order; //on table
    final StringProperty name;  //of var
    final StringProperty type;  //type of object
    final IntegerProperty xPos;
    final IntegerProperty yPos;

    public goLoItem() {
        order = new SimpleIntegerProperty(DEFAULT_ORDER);
        name = new SimpleStringProperty(DEFAULT_NAME);
        type = new SimpleStringProperty(DEFAULT_TYPE);
        xPos = new SimpleIntegerProperty(DEFAULT_X_POSITION);
        yPos = new SimpleIntegerProperty(DEFAULT_Y_POSITION);
        itemBorder = new goLoBorderStroke();
        itemRadialGradient = new goLoRadialGradient();
        itemFont = new goLoFont();

    }

    public goLoItem(int initOrder, String initName, String initType, int initXPos, int initYPos) {
        this();
        order.set(initOrder);
        name.set(initName);
        type.set(initType);
        xPos.set(initXPos);
        yPos.set(initYPos);

    }

    public goLoBorderStroke getGoLoBorder() {
        return itemBorder;
    }

    public void setGoLoBorder(goLoBorderStroke obj) {
        itemBorder = obj;
    }

    public abstract void initBorder();

    public goLoRadialGradient getGoLoRadGrad() {
        return itemRadialGradient;
    }

    public void setGoLoRadGrad(goLoRadialGradient obj) {
        itemRadialGradient = obj;
    }

    public abstract void initRadGrad();

    public goLoFont getGoLoFont() {
        return itemFont;
    }

    public void setGoLoFont(goLoFont obj) {
        itemFont = obj;
    }

    public abstract void initTextItem();

    public int getOrder() {
        return order.get();
    }

    public void setOrder(int value) {
        order.set(value);
    }

    public IntegerProperty orderProperty() {
        return order;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String value) {
        name.set(value);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getType() {
        return type.get();
    }

    public void setType(String value) {
        type.set(value);
    }

    public StringProperty typeProperty() {
        return type;
    }

    public int getXPos() {
        return xPos.get();
    }

    public void setXPos(int value) {
        xPos.set(value);
    }

    public IntegerProperty xPosProperty() {
        return xPos;
    }

    public int getYPos() {
        return yPos.get();
    }

    public void setYPos(int value) {
        yPos.set(value);
    }

    public IntegerProperty yPosProperty() {
        return yPos;
    }

    public abstract Object clone();

    public abstract void resize(double prefWidth, double prefHeight);

}
