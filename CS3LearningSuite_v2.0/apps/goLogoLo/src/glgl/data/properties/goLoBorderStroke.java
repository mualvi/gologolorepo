/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data.properties;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;

/**
 *
 * @author musta
 */
public class goLoBorderStroke {

    //Border Vars
    final DoubleProperty borderThickness;
    final StringProperty borderColor;
    final DoubleProperty borderRadius;

    public goLoBorderStroke() {

        //Border Vars
        borderThickness = new SimpleDoubleProperty(0);
        borderColor = new SimpleStringProperty("white");
        borderRadius = new SimpleDoubleProperty(0);
    }

    public void setUpBorder(Node n) {
        n.setStyle("-fx-border-width:" + getBorderThickness() + ";"
                + "-fx-border-radius:" + getBorderRadius() + ";"
                + "-fx-border-color:" + getBorderThickness() + ";"
        );

    }

    public double getBorderThickness() {
        return borderThickness.get();
    }

    public goLoBorderStroke setBorderThickness(double value) {
        borderThickness.set(value);
        return this;
    }

    public DoubleProperty borderThicknessProperty() {
        return borderThickness;
    }

    public String getBorderColor() {
        return borderColor.get();
    }

    public goLoBorderStroke setBorderColor(String value) {
        borderColor.set(value);
        return this;
    }

    public StringProperty borderColorProperty() {
        return borderColor;
    }

    public double getBorderRadius() {
        return borderRadius.get();
    }

    public goLoBorderStroke setBorderRadius(double value) {
        borderRadius.set(value);
        return this;
    }

    public DoubleProperty borderRadiusProperty() {
        return borderRadius;
    }

}
