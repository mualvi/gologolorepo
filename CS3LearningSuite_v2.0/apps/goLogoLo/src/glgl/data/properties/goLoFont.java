/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data.properties;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

/**
 *
 * @author musta
 */
public class goLoFont {

    final StringProperty font;
    final IntegerProperty fontSize;
    final BooleanProperty bolded;
    final BooleanProperty italicized;
    final BooleanProperty underlined;

    public goLoFont() {
        font = new SimpleStringProperty("Times New Roman");
        fontSize = new SimpleIntegerProperty(18);
        bolded = new SimpleBooleanProperty(false);
        italicized = new SimpleBooleanProperty(false);
        underlined = new SimpleBooleanProperty(false);

    }

    public Font makeFont() {
        Font fontForComponent = new Font(getFont(), getFontSize());

        if (getBolded() && getItalicized()) {
            fontForComponent = Font.font(getFont(), FontWeight.BOLD, FontPosture.ITALIC, getFontSize());
        } else if (!getBolded() && getItalicized()) {
            fontForComponent = Font.font(getFont(), FontWeight.NORMAL, FontPosture.ITALIC, getFontSize());
        } else if (getBolded() && !(getItalicized())) {
            fontForComponent = Font.font(getFont(), FontWeight.NORMAL, FontPosture.REGULAR, getFontSize());
        }

        return fontForComponent;
    }

    public String getFont() {
        return font.get();
    }

    public goLoFont setFont(String value) {
        font.set(value);
        return this;
    }

    public StringProperty fontProperty() {
        return font;
    }

    public int getFontSize() {
        return fontSize.get();
    }

    public goLoFont setFontSize(int value) {
        fontSize.set(value);
        return this;
    }

    public IntegerProperty fontSizeProperty() {
        return fontSize;
    }

    public boolean getBolded() {
        return bolded.get();
    }

    public goLoFont setBolded(boolean value) {
        bolded.set(value);
        return this;
    }

    public BooleanProperty boldedProperty() {
        return bolded;
    }

    public boolean getItalicized() {
        return italicized.get();
    }

    public goLoFont setItalicized(boolean value) {
        italicized.set(value);
        return this;
    }

    public BooleanProperty italicizedProperty() {
        return italicized;
    }

    public boolean getUnderlined() {
        return underlined.get();
    }

    public goLoFont setUnderlined(boolean value) {
        underlined.set(value);
        return this;
    }

    public BooleanProperty underlinedProperty() {
        return underlined;
    }

}
