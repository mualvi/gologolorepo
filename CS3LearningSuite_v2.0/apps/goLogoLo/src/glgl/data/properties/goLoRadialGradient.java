/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data.properties;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;

/**
 *
 * @author musta
 */
public class goLoRadialGradient {

    final DoubleProperty focusAngle;
    final DoubleProperty focusDistance;
    final DoubleProperty centerX;
    final DoubleProperty centerY;
    final DoubleProperty colorRadius;
    final StringProperty cycle;
    final StringProperty stop0;
    final StringProperty stop1;

    RadialGradient radialGradientField;

    public goLoRadialGradient() {
        focusAngle = new SimpleDoubleProperty(0);
        focusDistance = new SimpleDoubleProperty(0);
        centerX = new SimpleDoubleProperty(0);
        centerY = new SimpleDoubleProperty(0);
        colorRadius = new SimpleDoubleProperty(0);
        cycle = new SimpleStringProperty("NO_CYCLE");
        stop0 = new SimpleStringProperty("white");
        stop1 = new SimpleStringProperty("white");

    }

    public double getFocusAngle() {
        return focusAngle.get();
    }

    public goLoRadialGradient setFocusAngle(double value) {
        focusAngle.set(value);
        return this;
    }

    public DoubleProperty focusAngleProperty() {
        return focusAngle;
    }

    public double getFocusDistance() {
        return focusDistance.get();
    }

    public goLoRadialGradient setFocusDistance(double value) {
        focusDistance.set(value);
        return this;
    }

    public DoubleProperty focusDistanceProperty() {
        return focusDistance;
    }

    public double getCenterX() {
        return centerX.get();
    }

    public goLoRadialGradient setCenterX(double value) {
        centerX.set(value);
        return this;
    }

    public DoubleProperty centerXProperty() {
        return centerX;
    }

    public double getCenterY() {
        return centerY.get();
    }

    public goLoRadialGradient setCenterY(double value) {
        centerY.set(value);
        return this;
    }

    public DoubleProperty centerYProperty() {
        return centerY;
    }

    public double getColorRadius() {
        return colorRadius.get();
    }

    public goLoRadialGradient setColorRadius(double value) {
        colorRadius.set(value);
        return this;
    }

    public DoubleProperty colorRadiusProperty() {
        return colorRadius;
    }

    public String getCycle() {
        return cycle.get();
    }

    public goLoRadialGradient setCycle(String value) {
        cycle.set(value);
        return this;
    }

    public StringProperty cycleProperty() {
        return cycle;
    }

    public String getStop0() {
        return stop0.get();
    }

    public goLoRadialGradient setStop0(String value) {
        stop0.set(value);
        return this;
    }

    public StringProperty stop0Property() {
        return stop0;
    }

    public String getStop1() {
        return stop1.get();
    }

    public goLoRadialGradient setStop1(String value) {
        stop1.set(value);
        return this;
    }

    public StringProperty stop1Property() {
        return stop1;
    }

    public RadialGradient makeRadialGradient() {
        Stop zero = new Stop(.5, Color.valueOf(stop0.get()));
        Stop one = new Stop(.5, Color.valueOf(stop1.get()));

        return new RadialGradient(focusAngle.get(),
                focusDistance.get(),
                centerX.get(),
                centerY.get(),
                colorRadius.get(),
                true,
                CycleMethod.valueOf(cycle.get()),
                zero,
                one);
    }

}
