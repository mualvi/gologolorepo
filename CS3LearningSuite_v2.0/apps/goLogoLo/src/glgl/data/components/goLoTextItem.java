/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data.components;

import glgl.data.goLoItem;
import glgl.data.properties.goLoBorderStroke;
import glgl.data.properties.goLoFont;
import glgl.data.properties.goLoRadialGradient;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

/**
 *
 * @author musta
 */
public class goLoTextItem extends goLoItem {

    Text a;
    String stringForm;
    public static final int DEFAULT_X_Y_POSITION = 100;

    //ADD ITEM CONSTRUCTOR
    public goLoTextItem(String input) {
        a = new Text(DEFAULT_X_Y_POSITION, DEFAULT_X_Y_POSITION, input);
        a.setFill(Paint.valueOf("black"));
        stringForm = input;
    }

    //LOADING CONSTRUCTOR
    public goLoTextItem(int order, String name, String type, int xPos, int yPos, goLoRadialGradient initradial, goLoBorderStroke initborder) {
        super(order, name, type, xPos, yPos);
        a = new Text(xPos, yPos, name);
        stringForm = name;
        goLoFont goLoFont = getGoLoFont();
        a.setFont(goLoFont.makeFont());
        a.setUnderline(goLoFont.getUnderlined());

        a.setFill(Paint.valueOf("black"));

//      setGoLoRadGrad(initradial);
//      initRadGrad(); 
//      initborder.setUpBorder(a);  
    }

    public void initTextItem() {
        goLoFont goLoFont = this.getGoLoFont();
        a.setFont(goLoFont.makeFont());
        a.setUnderline(goLoFont.getUnderlined());
        a.setFill(Paint.valueOf("black"));

    }

    public Text getText() {
        return a;
    }

    public String toString() {
        return stringForm;
    }

    @Override
    public Text getComponent() {
        return a;
    }

    @Override
    public Object clone() {
        return new goLoTextItem(getOrder(), getName(), getType(), getXPos(), getYPos(), getGoLoRadGrad(), getGoLoBorder());
    }

    @Override
    public void initDrag(MouseEvent e) {
        setXY(e.getX(), e.getY());;
    }

    @Override
    public void setXY(double x, double y) {
        a.setX(x);
        a.setY(y);
    }

    @Override
    public void initRadGrad() {
        System.out.print("Not supported for text items");
    }

    @Override
    public void initBorder() {
        System.out.print("Not supported for text items");
    }

    @Override
    public void resize(double prefWidth, double prefHeight) {
        getComponent().resize(prefWidth, prefHeight);
    }
}
