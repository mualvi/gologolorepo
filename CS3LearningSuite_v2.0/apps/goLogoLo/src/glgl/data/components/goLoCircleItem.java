/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data.components;

import glgl.data.goLoItem;
import glgl.data.properties.goLoBorderStroke;
import glgl.data.properties.goLoRadialGradient;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

/**
 *
 * @author musta
 */
public class goLoCircleItem extends goLoItem {

    Circle circle;

    public goLoCircleItem() {
        circle = new Circle(0, 0, 100, Paint.valueOf("blue"));
    }

    public goLoCircleItem(int order, String name, String type, int xPos, int yPos, goLoRadialGradient initradial, goLoBorderStroke initborder) {
        super(order, name, type, xPos, yPos);
        circle = new Circle(100, 100, 100);
        setGoLoRadGrad(initradial);
        initRadGrad();
        initBorder();
    }

    @Override
    public Node getComponent() {
        return circle;
    }

    @Override
    public Object clone() {
        goLoCircleItem clone=  new goLoCircleItem(getOrder(), getName(), getType(), getXPos(), getYPos(), getGoLoRadGrad(), getGoLoBorder());
        clone.initBorder();
        clone.initRadGrad();
        return clone;
    }

    @Override
    public void setXY(double x, double y) {
        circle.setCenterX(x);
        circle.setCenterY(y);
    }

    @Override
    public void initDrag(MouseEvent e) {
        circle.setCenterX(e.getX());
        circle.setCenterY(e.getY());
    }

    @Override
    public void initRadGrad() {
        circle.setFill(getGoLoRadGrad().makeRadialGradient());
    }

    @Override
    public void initBorder() {
        circle.setStroke(Paint.valueOf(getGoLoBorder().getBorderColor()));
        circle.setStrokeWidth(getGoLoBorder().getBorderThickness());
        circle.setStrokeMiterLimit(getGoLoBorder().getBorderRadius());
    }

    @Override
    public void initTextItem() {
        System.out.println("Not supported.");
    }

    @Override
    public void resize(double prefWidth, double prefHeight) {
        getComponent().resize(prefWidth, prefHeight);
    }

}
