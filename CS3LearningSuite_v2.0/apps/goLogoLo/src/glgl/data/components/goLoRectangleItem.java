/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data.components;

import glgl.data.goLoItem;
import glgl.data.properties.goLoBorderStroke;
import glgl.data.properties.goLoRadialGradient;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author musta
 */
public class goLoRectangleItem extends goLoItem {

    Rectangle x;

    //   int length, width;
    public goLoRectangleItem() {
        x = new Rectangle(200, 200, Paint.valueOf("red"));
    }

    public goLoRectangleItem(int order, String name, String type, int xPos, int yPos, goLoRadialGradient initradial, goLoBorderStroke initborder) {
        super(order, name, type, xPos, yPos);
        x = new Rectangle(200, 200);
        setGoLoRadGrad(initradial);
        initRadGrad();
        initBorder();
    }

    @Override
    public Rectangle getComponent() {
        return x;
    }

    @Override
    public Object clone() {
        return new goLoRectangleItem(getOrder(), getName(), getType(), getXPos(), getYPos(), getGoLoRadGrad(), getGoLoBorder());
    }

    @Override
    public void initDrag(MouseEvent e) {
        setXY(e.getX(), e.getY());
    }

    @Override
    public void setXY(double xVal, double yVal) {
        x.setX(xVal);
        x.setY(yVal);
    }

    @Override
    public void initRadGrad() {
        x.setFill(getGoLoRadGrad().makeRadialGradient());
    }

    @Override
    public void initBorder() {
        x.setStroke(Paint.valueOf(getGoLoBorder().getBorderColor()));
        x.setStrokeWidth(getGoLoBorder().getBorderThickness());
        x.setStrokeMiterLimit(getGoLoBorder().getBorderRadius());
        //x.setStyle( "-fx-border-radius: " + getGoLoBorder().getBorderRadius() + ";");
    }

    @Override
    public void initTextItem() {
        System.out.println("Not supported.");
    }

    @Override
    public void resize(double prefWidth, double prefHeight) {
        getComponent().resize(prefWidth, prefHeight);
    }

}
