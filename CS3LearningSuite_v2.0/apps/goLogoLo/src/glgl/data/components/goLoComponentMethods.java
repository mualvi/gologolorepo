/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.data.components;

import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author musta
 */
public interface goLoComponentMethods {

    public Node getComponent();

    public void initDrag(MouseEvent e);

    public void setXY(double x, double y);

}
