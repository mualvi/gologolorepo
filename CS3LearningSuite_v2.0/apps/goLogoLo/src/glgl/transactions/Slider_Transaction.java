/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import glgl.workspace.goLoWorkspace;
import javafx.scene.control.Slider;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class Slider_Transaction implements jTPS_Transaction {

    goLogoLoApp app;
    goLoItem item;
    String controlType;
    Slider slider;
    double OGvalue;
    double newValue;

    public Slider_Transaction(goLogoLoApp initApp, goLoItem item, String controlType, double newValue, Slider slider) {
        app = initApp;
        this.item = item;
        this.controlType = controlType;
        this.newValue = newValue;
        this.slider = slider;
        OGvalue = ((goLoWorkspace) (app.getWorkspaceComponent())).getOGvalue();

    }

    @Override
    public void doTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.editSliderValue(item, controlType, newValue, slider);

    }

    @Override
    public void undoTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.editSliderValue(item, controlType, OGvalue, slider);

    }

}
