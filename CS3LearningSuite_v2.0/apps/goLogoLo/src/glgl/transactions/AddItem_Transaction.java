package glgl.transactions;

import jtps.jTPS_Transaction;
import glgl.data.goLoData;
import glgl.data.goLoItem;

/**
 *
 * @author Mufasa
 */
public class AddItem_Transaction implements jTPS_Transaction {

    goLoData data;
    goLoItem itemToAdd;

    public AddItem_Transaction(goLoData initData, goLoItem initNewItem) {
        data = initData;
        itemToAdd = initNewItem;
    }

    @Override
    public void doTransaction() {
        data.addItem(itemToAdd);
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
    }
}
