/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class Boolean_Transaction implements jTPS_Transaction {

    String controlSource;
    goLogoLoApp app;
    goLoItem item;

    public Boolean_Transaction(goLogoLoApp app, goLoItem item, String controlSource) {
        this.app = app;
        this.item = item;
        this.controlSource = controlSource;
    }

    @Override
    public void doTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.handleBooleanButtons(item, controlSource);

    }

    @Override
    public void undoTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.handleBooleanButtons(item, controlSource);

    }

}
