/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class EditName_Transaction implements jTPS_Transaction {

    goLogoLoApp app;
    goLoData data;
    goLoItem item;
    String newName;
    String OGname;

    public EditName_Transaction(goLogoLoApp initApp, goLoItem initItem, String initNewName) {
        app = initApp;
        item = initItem;
        newName = initNewName;
        OGname = initItem.getName();
    }

    @Override
    public void doTransaction() {
        data = (goLoData) app.getDataComponent();
        data.editItemName(item, newName);
    }

    @Override
    public void undoTransaction() {
        data = (goLoData) app.getDataComponent();
        data.editItemName(item, OGname);
    }
}
