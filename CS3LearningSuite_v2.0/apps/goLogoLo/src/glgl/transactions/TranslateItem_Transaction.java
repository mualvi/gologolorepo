/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.components.goLoComponentMethods;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class TranslateItem_Transaction implements jTPS_Transaction {

    goLoComponentMethods component;
    double startX;
    double startY;
    double endX;
    double endY;

    public TranslateItem_Transaction(goLoComponentMethods initComponent, double startX, double startY, double endX, double endY) {
        component = initComponent;
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;

    }

    @Override
    public void doTransaction() {

        component.setXY(endX, endY);

    }

    @Override
    public void undoTransaction() {
        component.setXY(startX, startY);
    }

}
