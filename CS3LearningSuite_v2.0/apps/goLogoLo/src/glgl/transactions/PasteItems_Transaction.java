package glgl.transactions;

import jtps.jTPS_Transaction;
import glgl.goLogoLoApp;
import glgl.data.goLoData;
import glgl.data.goLoItem;

/**
 *
 * @author McKillaGorilla
 */
public class PasteItems_Transaction implements jTPS_Transaction {

    goLogoLoApp app;
    goLoItem itemToPaste;

    public PasteItems_Transaction(goLogoLoApp initApp, goLoItem initItemToPaste) {
        app = initApp;
        itemToPaste = initItemToPaste;
    }

    @Override
    public void doTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.addItem(itemToPaste);

    }

    @Override
    public void undoTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.removeItem(itemToPaste);
    }
}
