package glgl.transactions;

import jtps.jTPS_Transaction;
import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;

/**
 *
 * @author McKillaGorilla
 */
public class RemoveItem_Transaction implements jTPS_Transaction {

    goLogoLoApp app;
    goLoItem itemToRemove;

    public RemoveItem_Transaction(goLogoLoApp initApp, goLoItem initItem) {
        app = initApp;
        itemToRemove = initItem;
    }

    @Override
    public void doTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.removeItem(itemToRemove);
    }

    @Override
    public void undoTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.addItem(itemToRemove);
    }
}
