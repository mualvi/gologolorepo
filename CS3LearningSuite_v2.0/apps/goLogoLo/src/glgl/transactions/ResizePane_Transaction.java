/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class ResizePane_Transaction implements jTPS_Transaction {

    goLoData data;
    Pane p;
    double newWidth, newHeight, oldWidth, oldHeight;

    public ResizePane_Transaction(goLoData data, Pane p, double width, double height) {
        this.data = data;
        this.p = p;
        this.newWidth = width;
        this.newHeight = height;
        oldWidth = p.getScaleY();
        oldHeight = p.getScaleY();
    }

    @Override
    public void doTransaction() {
        data.resizePane(p, newWidth, newHeight);
    }

    @Override
    public void undoTransaction() {
        data.resizePane(p, oldWidth, oldHeight);
    }

}
