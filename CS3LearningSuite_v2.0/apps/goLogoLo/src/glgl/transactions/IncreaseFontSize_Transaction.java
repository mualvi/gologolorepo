/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import glgl.data.goLoItem;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class IncreaseFontSize_Transaction implements jTPS_Transaction {

    goLoData data;
    goLoItem item;

    public IncreaseFontSize_Transaction(goLoData initData, goLoItem initItem) {
        data = initData;
        item = initItem;
    }

    @Override
    public void doTransaction() {
        data.increaseFontSize(item);

    }

    @Override
    public void undoTransaction() {
        data.decreaseFontSize(item);
    }

}
