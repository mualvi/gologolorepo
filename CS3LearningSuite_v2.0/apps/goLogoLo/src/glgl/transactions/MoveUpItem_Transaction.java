/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class MoveUpItem_Transaction implements jTPS_Transaction {

    goLogoLoApp app;
    goLoData data;
    goLoItem toMoveUpItem;

    public MoveUpItem_Transaction(goLogoLoApp initApp, goLoItem initMoveUpItem) {
        app = initApp;
        toMoveUpItem = initMoveUpItem;
    }

    @Override
    public void doTransaction() {
        data = (goLoData) app.getDataComponent();
        data.moveUpItem(toMoveUpItem);
    }

    @Override
    public void undoTransaction() {
        data = (goLoData) app.getDataComponent();
        data.moveDownItem(toMoveUpItem);
    }
}
