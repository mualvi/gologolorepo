/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class EditTextItem_Transaction implements jTPS_Transaction {

    goLoData data;
    String newStringForTextItem;
    String originalText;

    public EditTextItem_Transaction(goLoData initData, goLoItem item, String newStringForTextItem, String OG) {
        data = initData;
        this.newStringForTextItem = newStringForTextItem;
        this.originalText = OG; 
    }

    @Override
    public void doTransaction() {
        data.editTextItem(newStringForTextItem);
    }

    @Override
    public void undoTransaction() {
        data.editTextItem(originalText);
    }
}
