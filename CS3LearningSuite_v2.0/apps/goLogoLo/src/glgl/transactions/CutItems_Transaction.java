package glgl.transactions;

import jtps.jTPS_Transaction;
import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import glgl.goLogoLoApp;
import glgl.data.goLoData;
import glgl.data.goLoItem;

/**
 *
 * @author McKillaGorilla
 */
public class CutItems_Transaction implements jTPS_Transaction {

    goLogoLoApp app;
    goLoItem itemToCut;
    int cutItemLocation;

    public CutItems_Transaction(goLogoLoApp initApp, goLoItem initItemsToCut) {
        app = initApp;
        itemToCut = initItemsToCut;
    }

    @Override
    public void doTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        cutItemLocation = data.removeItemGetIndex(itemToCut);

        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }

    @Override
    public void undoTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.addItemAt(itemToCut, cutItemLocation);

        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }
}
