/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glgl.transactions;

import glgl.data.goLoData;
import glgl.data.goLoItem;
import glgl.goLogoLoApp;
import javafx.scene.control.ComboBoxBase;
import jtps.jTPS_Transaction;

/**
 *
 * @author musta
 */
public class Choice_Transaction implements jTPS_Transaction {

    goLogoLoApp app;
    goLoItem item;
    String controlType;
    ComboBoxBase box;
    String OGvalue;
    String newValue;

    public Choice_Transaction(goLogoLoApp initApp, goLoItem item, String controlType, String newValue, ComboBoxBase box) {
        app = initApp;
        this.item = item;
        this.controlType = controlType;
        this.newValue = newValue;
        this.box = box;

        switch (controlType) {
            case "borderColor":
                OGvalue = item.getGoLoBorder().getBorderColor();
                break;
            case "cycleMethod":
                OGvalue = item.getGoLoRadGrad().getCycle();
                break;
            case "stop0":
                OGvalue = item.getGoLoRadGrad().getStop0();
                break;
            case "stop1":
                OGvalue = item.getGoLoRadGrad().getStop1();
                break;
            case "fontStyle":
                OGvalue = item.getGoLoFont().getFont();
                break;
            case "fontSize":
                OGvalue = "" + item.getGoLoFont().getFontSize();
                break;

        }
    }

    @Override
    public void doTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.editChoiceValue(item, controlType, newValue, box);

    }

    @Override
    public void undoTransaction() {
        goLoData data = (goLoData) app.getDataComponent();
        data.editChoiceValue(item, controlType, OGvalue, box);

    }

}
